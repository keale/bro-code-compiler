
	\c
	 c Simulates a logic circuit's behavior using 
	 c the given logical operators. Logic circuit 
	 c = [(A and B) nor (C or D)] nand (not E)
	 c\

progname #circuitSim
commence(:
	tnf $A, $B, $C, $D, $E\e

	$A = false\e

	println("Give the value of B.[true\false]")\e
	get $B\e

	println("Give the value of C.[true\false]")\e
	get $C\e

	$D = true\e

	println("Give the value of E.[true\false]")\e
	get $E\e
        
	tnf $result = ($A and $B | ($C or $D)) & ~$E\e

	println("The result was ".$result)\e
:)
end
