
	\c 
	 c Get's the coordinates and tells in which
	 c quadrant the coordinates belong.
	 c\

progname #whichQuad
commence (:
	num $x_point, $y_point\e


	\\get x-coordinate
	println("Enter the x-coordinate.")\e
	get $x_point\e


	\\get y-coordinate
	println("Enter the y-coordinate.")\e
	get $y_point\e

	text $answer\e
	
        if ($x_point == 0 & $y_point == 0)
	then (:
		$answer = "The point is the Origin."\e
	:)
	else (:
		if ($x_point ~= 0 & $y_point == 0)
		then (:
			$answer = "The point is on the X-Axis."\e
		:)
		else (:
			if ($x_point == 0 & $y_point ~= 0)
			then (:
				$answer = "The point is on the Y-Axis."\e
			:)
			else (:
				if ($x_point > 0 & $y_point > 0)
				then (:
					$answer = "The point is on Quadrant I."\e
				:)
				else (:
					if ($x_point < 0 & $y_point > 0)
					then (:
						$answer = "The point is on Quadrant II."\e
					:)
					else (:
						if ($x_point < 0 & $y_point < 0)
						then (:
							$answer = "The point is on Quadrant III."\e
						:)
						else (:
							if ($x_point > 0 & $y_point < 0)
							then (:
								$answer = "The point is on Quadrant IV."\e
							:)
						:)
					:)
				:)
			:)
		:)
	:)

	println($answer)\e
	
:)
end
