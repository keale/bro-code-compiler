progname #sampleProg1
commence
(:
    text $input \e
    text $choice \e
    text $repeat \e
    num $decNum \e
    bin $binNum \e
    oct $octNum \e
    hex $hexNum \e

    do
    (:
        print("Enter a number: ")\e
        get $input \e
        println("Convert to:")\e
        println("a. binary")\e
        println("b. octal")\e
        println("c. hexadecimal")\e
        print("Enter choice: ")\e
        get $choice\e

        check ($choice)
            when "a"
            (:
                $decNum = tonum $input\e
                $binNum = tobin $decNum\e
                println("When converted to binary, the value is ".$binNum)\e
                print("Would you like to try again (y/n)? ")\e
                get $repeat\e
            :)stop\e

            when "b"
            (:
                $decNum = tonum $input\e
                $octNum = tooct $decNum\e
                println("When converted to octal, the value is ".$octNum)\e
                print("Would you like to try again (y/n)? ")\e
                get $repeat\e
            :)stop\e

            when "c"
            (:
                $decNum = tonum $input\e
                $hexNum = tohex $decNum\e
                println("When converted to hexadecimal, the value is ".$hexNum)\e
                print("Would you like to try again (y/n)? ")\e
                get $repeat\e
            :)stop\e

            etc
            (:
                println("Input is in none of the choices.")\e 
                print("Would you like to try again (y/n)?")\e
                get $repeat\e
            :)stop\e
    :)while($choice=="y")
:)
end