progname #repeat
commence
(:
    text $input1, $choice="y"\e
    num $ctr, $input2\e

    while($choice=="y")\e
    (:
        print("Enter a word: ")\e
        get $input1\e
        print("How many times do you like to output ".$input1."? ")\e
        get $input2\e

        for($ctr=1, $ctr<=input2, inc $ctr by 1)
        (:
            println("Loop ".$ctr.": ".$input1)\e
        :)

        print("Do you like to try again (y/n)? ")\e
        get $choice\e
    :)
:)
end