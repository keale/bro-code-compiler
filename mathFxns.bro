
	\c 
	 c Gets two numbers from user and prints the resulting
	 c number using the four fundamental functions
	 c\

progname #mathFxns
commence (:
	num $n1, $n2\e

	println("Give number 1")\e
	get $n1\e

	println("Give number 2")\e
	get $n2\e

	num $res\e

	$res = $n1 + $n2\e
	println("Result of addition is: ".$res)\e

	$res = $n1 - $n2\e
	println("Result of subtraction is: ".$res)\e

	$res = $n1 * $n2\e
	println("Result of multiplication is: ".$res)\e

	$res = $n1 / $n2\e
	println("Result of division is: ".$res)\e

	$res = $n1 ^ $n2\e
	println("Result of exponential is: ".$res)\e
:)
end
