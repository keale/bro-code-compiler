package analyzer;


public class CFG {
	
	private GrammarRule[] rules;
	private String start;

	public static final String VAR_AUGMENTED_START = "[S']";

	public CFG(String start, GrammarRule[] rules){
		this.rules = rules;
		this.start = start;
		
	}
	
	public GrammarRule[] rules(){
		return rules;
	}
	
	public void setGammarRules(GrammarRule[] rules){
		this.rules = rules;
	}
	
	public String startVariable(){
		return start;
	}
	
	public void setStartVariable(String start){
		this.start = start;
	}
	
	public GrammarRule rule(int ix){
		for(int i = 0; i < rules.length; i++){
			if(ix == i) return rules[i];
		}
		return null;
	}

	public void augmentGrammar(){
		GrammarRule[] rules = new GrammarRule[this.rules.length + 1];
		
		rules[0] = new GrammarRule(VAR_AUGMENTED_START, new String[] {GrammarRule.VAR_START});//, Token.TYPE_EOF});
		
		for(int i = 1; i < rules.length; i++){
			rules[i] = this.rules[i-1];
		}

		this.start = VAR_AUGMENTED_START;
		this.rules = rules;
	}
}
