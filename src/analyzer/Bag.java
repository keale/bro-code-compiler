package analyzer;

import jankiel.utils.TreeNode;
import scanner.Token;

public class Bag{
	
	private String str;
	private TreeNode node;
	private String state;
	
	public Bag(String str, TreeNode n, String s){
		this.str = str;
		node = n;
		state = s;
	}
	
	public String state(){
		return state;
	}
	
	public TreeNode node(){
		return node;
	}
	
	public String str(){
		return str;
	}
	
	@Override
	public String toString() {
		return str + " " + state;
	}
}
