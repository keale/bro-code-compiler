/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package analyzer;

/**
 *
 * @author ust
 */
public class SLLNode<T> {
    public T info;
    public SLLNode<T> next;
    public SLLNode() { // constructs empty node
        next = null; }
    public SLLNode(T el) { // constructs node with data
        info = el;
        next = null; }
    public SLLNode(T el, SLLNode<T> ptr) {
        // node with data pointing to other pre-existing node
        info = el;
        next = ptr;
    }
    public T info(){
        return info;
    }
    public SLLNode<T> next(){
        return next;
    }
}