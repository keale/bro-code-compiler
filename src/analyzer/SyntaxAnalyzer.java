package analyzer;

import jankiel.utils.SLL;
import jankiel.utils.Tree;
import jankiel.utils.TreeNode;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Stack;

import scanner.DFA;
import scanner.LexicalScanner;
import scanner.Token;

public class SyntaxAnalyzer {

    private LexicalScanner scanner;
    private DFA tokenDfa;
    
    private DFA stateDfa;
    
    private CFG grammar;
    private Stack<Bag> stack;
    
    private Tree<Token> parseTree;
    private TreeNode<Token> root;
    
    private String[][] parsingTable;
    private String[] header;
    
    private ArrayList<Token> allTokens;
    
    private boolean errorRaised;
    private int currentLine;
    private int numOfErrors;
    
    private boolean verboseMode;

    public SyntaxAnalyzer(String filename, boolean verbose)
            throws FileNotFoundException {

        currentLine = 0;
        numOfErrors = 0;
        errorRaised = false;
        
        verboseMode = verbose;

        SymbolTable.table = new HashMap<String, Token>();

        String start = "START";
        String[][] table = tokenTable(start);
        String[] finals = tokenFinalStates();
        tokenDfa = new DFA(start, table, finals);

        scanner = new LexicalScanner(filename, tokenDfa);

        allTokens = new ArrayList<Token>();

        start = GrammarRule.VAR_START;
        GrammarRule[] rules = cfgRules(start);
        grammar = new CFG(start, rules);

        grammar.augmentGrammar();

        start = "s0";
        table = stateTable(start);

        stateDfa = new DFA(start, table, new String[]{""});
        stateDfa.restart();

        stack = new Stack();

        TreeNode node = new TreeNode<Token>(new Token(Token.TYPE_EOF, "\\F"));
        stack.push(new Bag(Token.TYPE_EOF, node, stateDfa.state()));

        initParsingTable();

    }

    public Tree<Token> analyzeSyntax() throws FileNotFoundException {
        
        if(verboseMode)
            System.out.println("Brisk, Rudimentary and Opportune Code");
        
        if(verboseMode)
            System.out.println("Scanning source file for tokens...");

        while (scanner.hasNext()) {

            Token currentToken = scanner.getToken();
            
            if(verboseMode)
                System.out.println(currentToken.toString());

            if (currentToken.lexeme().equals("\\L")) {
                allTokens.add(new Token(Token.SYNTAX_NEXT_LINE,
                        Integer.toString(scanner.lineNumber() + 1)));
            }

            putIntable(currentToken);

            if (currentToken.type().equals(Token.TYPE_NOISE)
                    || currentToken.type().equals(Token.TYPE_COMMENT_MULTI)
                    || currentToken.type().equals(Token.TYPE_COMMENT_SINGLE)
                    || currentToken.type().equals(Token.TYPE_SPACE)
                    || currentToken.type().equals(Token.TYPE_ERROR)) {
                continue;
            }

            allTokens.add(currentToken);
        }

        if(verboseMode)
            System.out.println("Scanning complete!");

        if(verboseMode)
            System.out.println("Analyzing the syntax of source file...");

        if(verboseMode)
            System.out.println("Stack: " + stack.toString());

        for (int i = 0; i < allTokens.size(); i++) {

            String state = stack.peek().state();
            Token currentToken = allTokens.get(i);

            String todo = lookUpTable(state, currentToken.type());
            char action = todo.charAt(0);

            if (currentToken.type().equals(Token.SYNTAX_NEXT_LINE)) {
                currentLine = Integer.parseInt(currentToken.lexeme());
                continue;

            }

            if (todo.equals("r57s175")) {
                reduce(grammar.rule(57));
                shift(currentToken, "S175");

            } else {

                switch (action) {
                    case 's':
                        shift(currentToken, "S" + todo.substring(1));
                        break;
                    case 'r': {
                        int ruleIx = 0;
                        try {
                            ruleIx = Integer.parseInt(todo.substring(1));
                        } catch (Exception e) {
                            System.out.println(todo + currentToken);
                        }

                        reduce(grammar.rule(ruleIx));
                        i--;
                    }
                    break;
                    case 'a':
                        accept();
                        break;
                    case 'e':
                        raiseError(state, currentToken);
                        break;
                }
            }
        }

        if(verboseMode)
            System.out.println("Analysis complete!");
        
        if(verboseMode)
            System.out.println(SymbolTable.table.toString());

        return parseTree;
    }

    public void shift(Token token, String state) {

        if(verboseMode)
            System.out.println("At state " + stateDfa.state() + ", shift "
                + token.lexeme() + " and go to state " + state + ".");

        stateDfa.setState(state);

        TreeNode<Token> node = new TreeNode<Token>(token);

        Bag bag = new Bag(token.type(), node, stateDfa.state());
        stack.push(bag);

        if(verboseMode)
            System.out.println("Stack: " + stack.toString());

    }

    public void reduce(GrammarRule rule) {

        if(verboseMode)
            System.out.print(rule.leftHandSide() + " was seen. Reduce by rule: ");

        if(verboseMode)
            System.out.println(rule.rule());

        ArrayList<Bag> items = new ArrayList<Bag>();

        for (int i = 0; i < rule.rightHandSide().length
                && !rule.rightHandSide()[0].equals(""); i++) {
            items.add(stack.pop());

            stateDfa.setState(stack.peek().state());
        }

        if (rule.rightHandSide()[0].equals("")) {
            stateDfa.setState(stack.peek().state());
        }

        String variable = rule.leftHandSide();
        String gotoState = "S" + lookUpTable(stateDfa.state(), variable);
        TreeNode<Token> parent = new TreeNode<Token>(rule);

        Bag item = new Bag(variable, parent,
                gotoState.substring(0));

        stack.push(item);

        if(verboseMode)
            System.out.println("And go to state " + gotoState + "; current state " + stateDfa.state());
        
        if(verboseMode)
            System.out.println("Stack: " + stack.toString());

        SLL<TreeNode<Token>> children = new SLL<TreeNode<Token>>();

        for (int i = items.size() - 1; i >= 0 && items.size() > 0; i--) {
            children.addToTail(items.get(i).node());
        }

        parent.setChildren(children);

        root = parent;
    }

    public void accept() {
        if (errorRaised) {
            if(verboseMode)
                System.out.println("Analyzer has detected "
                        + numOfErrors + " error(s). Fix them first.");
        } else {
            if(verboseMode)
                System.out.println("Analyzer has accepted "
                        + "the syntax of the source code.");
        }

        parseTree = new Tree<Token>(root);
    }

    public void raiseError(String state, Token current) {
        errorRaised = true;
        numOfErrors++;

        StringBuffer expectedTokens = new StringBuffer("");

        String[] row = parsingTable[getRowIndex(state)];

        String toBeExpected = Token.sampleLexeme(parsingTable[0][1]);

        int firstItem = 0;

        for (int i = 0; i < row.length; i++) {

            toBeExpected = Token.sampleLexeme(parsingTable[0][i]);

            if (!row[i].equals("e") && !toBeExpected.equals("")) {
                expectedTokens.append(toBeExpected);

                firstItem = i;
                break;
            }
        }

        if (!row[0].equals("e") && !toBeExpected.equals("")) {
            expectedTokens.append(toBeExpected);
        }

        for (int i = firstItem + 1; i < row.length; i++) {
            if (parsingTable[0][i].charAt(0) == '[') {
                break;
            }

            toBeExpected = Token.sampleLexeme(parsingTable[0][i]);

            if (!row[i].equals("e") && !toBeExpected.equals("")) {
                expectedTokens.append(" or ").append(toBeExpected);
            }
        }

        System.out.println("Error! Unexpected \"" + current.lexeme()
                + "\". Expecting " + expectedTokens + " at line " + currentLine);
    }

    public Tree<Token> getparseTree() {
        return parseTree;
    }

    public String lookUpTable(String state, String itemInStack) {

        try {
            return parsingTable[getRowIndex(state)][getColumnIndex(itemInStack)];
        } catch (Exception e) {
        }

        return "e";
    }

    public void putIntable(Token token) {
        String lexeme = token.lexeme();

        if (token.type().equals(Token.TYPE_IDENTIFIER)
                || token.type().equals(Token.TYPE_ARRAY_IDENTIFIER)) {

            if (!SymbolTable.table.containsKey(lexeme)) {
                SymbolTable.table.put(lexeme, token);
                
            }

        } else if (token.type().equals(Token.TYPE_NOISE)
                || token.type().equals(Token.TYPE_COMMENT_MULTI)
                || token.type().equals(Token.TYPE_COMMENT_SINGLE)
                || token.type().equals(Token.TYPE_SPACE)) {
            //do nothing
            
        } else if (token.type().equals(Token.TYPE_ERROR)) {
            System.out.println(token.toString());

        } else if (token.type().equals(Token.TYPE_LITERAL)) {
        	
        	token.setLexeme(removeEscapeCharacters(lexeme));
            SymbolTable.table.put(lexeme, token);
            
            token.setIdType(Token.DATA_TYPE_TEXT);
            token.setIdValue(removeEscapeCharacters(lexeme));
            token.setPrintValue(removeEscapeCharacters(lexeme));

        } else if (token.type().equals(Token.TYPE_NUMERIC)
                || token.type().equals(Token.TYPE_MAYBE_OCT)
                || token.type().equals(Token.TYPE_MAYBE_BIN)
                || token.type().equals(Token.TYPE_NUMERIC_FLOAT)) {
            
            if(Double.parseDouble(lexeme) <= 2147483647){
                SymbolTable.table.put(lexeme, token);
            
                token.setIdType(Token.DATA_TYPE_NUM);
                token.setIdValue(lexeme);
                token.setPrintValue(lexeme);
            }
            else System.out.println("Error! Value too large!");

        } else if (token.type().equals(Token.TYPE_HEX_CONSTANT)) {
            SymbolTable.table.put(lexeme, token);
            
            token.setIdType(Token.DATA_TYPE_HEX);
            String toInsert = convert(10, lexeme.substring(0, lexeme.length()-1), 16);
            if(Double.parseDouble(toInsert) <= 2147483647){
                token.setIdValue(toInsert);
                token.setPrintValue(lexeme);
            }
            else System.out.println("Error! Value too large!");
            
        } else if (token.type().equals(Token.TYPE_OCT_CONSTANT)) {
            SymbolTable.table.put(lexeme, token);
            
            token.setIdType(Token.DATA_TYPE_OCT);
            String toInsert = convert(10, lexeme.substring(0, lexeme.length()-1), 8);
            if(Double.parseDouble(toInsert) <= 2147483647){
                token.setIdValue(toInsert);
                token.setPrintValue(lexeme);
            }
            else System.out.println("Error! Value too large!");
            
            
        } else if (token.type().equals(Token.TYPE_BIN_CONSTANT)) {
            SymbolTable.table.put(lexeme, token);
            
            token.setIdType(Token.DATA_TYPE_BIN);
            String toInsert = convert(10, lexeme.substring(0, lexeme.length()-1), 2);
            if(Double.parseDouble(toInsert) <= 2147483647){
                token.setIdValue(toInsert);
                token.setPrintValue(lexeme);
            }
            else System.out.println("Error! Value too large!");
            
            
        } else if (token.type().equals(Token.TYPE_TRUE)) {
            SymbolTable.table.put(lexeme, token);
            
            token.setIdType(Token.DATA_TYPE_TNF);
            token.setIdValue("true");
            token.setPrintValue("true");

        } else if (token.type().equals(Token.TYPE_FALSE)) {
            SymbolTable.table.put(lexeme, token);
            
            token.setIdType(Token.DATA_TYPE_TNF);
            token.setIdValue("false");
            token.setPrintValue("false");

        } else {
//            SymbolTable.table.put(lexeme, token);
        }
    }
    
    public String removeEscapeCharacters(String s){
    	StringBuffer newString = new StringBuffer("");
    	
    	for(int i = 0; i < s.length(); i++){
    		if(s.charAt(i) == '\\'){
    			newString.append(s.charAt(i+1));
    			i++;
    		} else newString.append(s.charAt(i));
    	}
    	
    	return newString.toString();
    }

    public int getColumnIndex(String str) {

        for (int i = 0; i < header.length; i++) {

            if (header[i].equals(str)) {
                return i;
            }
        }

        return -1;
    }

    public int getRowIndex(String str) {
        if (Character.isDigit(str.charAt(0))) {
            return Integer.parseInt(str) + 1;
        } else {
            return Integer.parseInt(str.substring(1)) + 1;
        }
    }
    
    public String convert(int baseToConvertTo, String input, int baseOfNumber) {
        int[] rem = new int[1000];
        int num, i, ans, base, j, b; String inp; double n = 0, bin = 0;
        StringBuffer sk = new StringBuffer("");
        base = baseToConvertTo;
        inp = input;
        b = baseOfNumber;
        num = 0;
        for (i = inp.length()-1; i >= 0; i--) {
            if (b >= 16){
                if(Character.isLetter(inp.charAt(i))){
                    switch (inp.charAt(i)){
                        case 'A': bin = 10; break;
                        case 'B': bin = 11; break;
                        case 'C': bin = 12; break;
                        case 'D': bin = 13; break;
                        case 'E': bin = 14; break;
                        case 'F': bin = 15; break;
                    }
                }
                if(Character.isDigit(inp.charAt(i)))
                    bin = (int)(inp.charAt(i))-48;
            }
            else bin = (int)(inp.charAt(i))-48;
                num += bin*Math.pow(b,n);
                n++;
        }
        //System.out.printf ("Number\tBase\tAnswer\tRemainder\n") ;
        ans = 2;
        for (i = 0; ans > 0; i++){
            ans = num/base;
            rem[i] = num%base;
            //System.out.printf ("%d\t\t%d\t\t%d\t\t%d\n", num, base, ans, rem[i]);
            num=ans;
        }
        i--;
        for (j = i; i >= 0; i--){
            if (rem[i]>9)
                switch (rem[i]){
                    case 10: sk.append("A"); break;
                    case 11: sk.append("B"); break;
                    case 12: sk.append("C"); break;
                    case 13: sk.append("D"); break;
                    case 14: sk.append("E"); break;
                    case 15: sk.append("F"); break;
                }
            else {
                sk.append(Integer.toString(rem[i]));
            }
        }
		
		return sk.toString();
    }

    public void initParsingTable() {
        header = new String[]{Token.TYPE_ADD_SUBTRACT, Token.TYPE_AND_NAND,
            Token.TYPE_ARRAY_DATA_TYPE, Token.TYPE_ASSIGNMENT,
            Token.TYPE_BREAK, Token.TYPE_EXPONENTIAL,
            Token.TYPE_CLOSE_PARENTHESIS, Token.TYPE_MULTIPLY_DIVIDE,
            Token.TYPE_DATA_TYPE, Token.TYPE_DELIMITER, Token.TYPE_DO,
            Token.TYPE_FOR, Token.TYPE_GET, Token.TYPE_CLOSE_PROGRAM_BLOCK,
            Token.TYPE_IDENTIFIER, Token.TYPE_IF, Token.TYPE_LITERAL,
            Token.TYPE_NUMERIC, Token.TYPE_OPEN_PARENTHESIS,
            Token.TYPE_OR_NOR, Token.TYPE_OUTPUT, Token.TYPE_PROGNAME,
            Token.TYPE_OPEN_PROGRAM_BLOCK, Token.TYPE_RELATIONAL,
            Token.TYPE_SWITCH, Token.TYPE_THEN, Token.TYPE_NUMERIC_FLOAT,
            Token.TYPE_TO_DATA_TYPE, Token.TYPE_WHILE, Token.TYPE_NOT,
            Token.TYPE_ELSE, Token.TYPE_COMMA, Token.TYPE_TRUE,
            Token.TYPE_FALSE, Token.TYPE_STRING_CONCAT,
            Token.TYPE_ARRAY_IDENTIFIER, Token.TYPE_ARRAY_CLOSING,
            Token.TYPE_OPEN_BRACES, Token.TYPE_CLOSE_BRACES,
            Token.TYPE_OPEN_BLOCK, Token.TYPE_ADD_SUBTRACT_INC,
            Token.TYPE_CLOSE_BLOCK, Token.TYPE_MAYBE_BIN, Token.TYPE_MAYBE_OCT,
            Token.TYPE_HEX_CONSTANT, Token.TYPE_OCT_CONSTANT,
            Token.TYPE_BIN_CONSTANT, Token.TYPE_ADD_SUBTRACT_DEC,
            Token.TYPE_OUTPUT_2, Token.TYPE_WHEN, Token.TYPE_ETC,
            Token.TYPE_PROGRAM_ID, Token.TYPE_EOF, GrammarRule.VAR_START,
            GrammarRule.VAR_ARRAY_CONTENTS, GrammarRule.VAR_ARRAY_CONTENTS_PUMPAH,
            GrammarRule.VAR_ARRAY_DECLARE_STMT, GrammarRule.VAR_ARRAY_ID,
            GrammarRule.VAR_ARRAY_INIT_STMT, GrammarRule.VAR_BLOCK,
            GrammarRule.VAR_BOOLEAN_AND, GrammarRule.VAR_BOOLEAN_EXP,
            GrammarRule.VAR_BOOLEAN_LGE, GrammarRule.VAR_BOOLEAN_NOT,
            GrammarRule.VAR_BOOLEAN_OR, GrammarRule.VAR_CONSTANT,
            GrammarRule.VAR_DECLARE_STMT, GrammarRule.VAR_DECLARE_STMT_PUMPAH,
            GrammarRule.VAR_DO_WHILE_STMT, GrammarRule.VAR_FOR_STMT,
            GrammarRule.VAR_GET_STMT, GrammarRule.VAR_IDENTIFIER,
            GrammarRule.VAR_IF_STMT, GrammarRule.VAR_INC_STMT, GrammarRule.VAR_INIT_STMT,
            GrammarRule.VAR_MATH_CAST, GrammarRule.VAR_MATH_EXP,
            GrammarRule.VAR_MATH_FACTOR, GrammarRule.VAR_MATH_POWER,
            GrammarRule.VAR_MATH_TERM, GrammarRule.VAR_NUMERIC_CONST,
            GrammarRule.VAR_PRINT_STMT, GrammarRule.VAR_STATEMENT,
            GrammarRule.VAR_STRING_CONCAT, GrammarRule.VAR_SWITCH_BLOCK,
            GrammarRule.VAR_SWITCH_STMT, GrammarRule.VAR_WHILE_STMT,
            GrammarRule.VAR_ELSE_STMT
        };

        parsingTable = new String[stateDfa.stateCount() + 1][header.length];

        parsingTable[0] = header;

        for (int i = 1; i < parsingTable.length; i++) {
            parsingTable[i] = new String[header.length];
        }

        for (int i = 1; i < parsingTable.length; i++) {
            for (int j = 0; j < parsingTable[i].length; j++) {
                parsingTable[i][j] = "";
            }
        }

        fill1();
        fill2();
        fill3();
        fill4();

        for (int i = 1; i < parsingTable.length; i++) {
            for (int j = 0; j < parsingTable[i].length; j++) {
                if (parsingTable[i][j].equals("")) {
                    parsingTable[i][j] = "e";
                }
            }
        }
    }

    public String[][] stateTable(String s) {
        String[][] table = new String[204][3];

        for (int i = 0; i < table.length; i++) {
            table[i] = new String[]{"S" + i, "", ""};
        }

        return table;
    }

    public void fill1() {
        //STATE 0
        parsingTable[getRowIndex("S0")][getColumnIndex(GrammarRule.VAR_START)] += "1";
        parsingTable[getRowIndex("S0")][getColumnIndex(GrammarRule.TYPE_PROGNAME)] += "s2";

        //STATE 1
        parsingTable[getRowIndex("S1")][getColumnIndex(GrammarRule.TYPE_EOF)] += "a ";

        //STATE 2
        parsingTable[getRowIndex("S2")][getColumnIndex(GrammarRule.TYPE_PROGRAM_ID)] += "s3";

        //STATE 3
        parsingTable[getRowIndex("S3")][getColumnIndex(GrammarRule.TYPE_OPEN_PROGRAM_BLOCK)] += "s4";

        //STATE 4
        parsingTable[getRowIndex("S4")][getColumnIndex(GrammarRule.VAR_BLOCK)] += "5";
        parsingTable[getRowIndex("S4")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "s7";

        //STATE 5
        parsingTable[getRowIndex("S5")][getColumnIndex(GrammarRule.TYPE_CLOSE_PROGRAM_BLOCK)] += "s6";

        //STATE 6
        parsingTable[getRowIndex("S6")][getColumnIndex(GrammarRule.TYPE_EOF)] += "r1";

        //STATE 7
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "8";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S7")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 8
        parsingTable[getRowIndex("S8")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "s9";

        //STATE 9
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_CLOSE_PROGRAM_BLOCK)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_ELSE)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_IF)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_DO)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_GET)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r2";
        parsingTable[getRowIndex("S9")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r2";

        //STATE 10
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "11";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S10")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 11
        parsingTable[getRowIndex("S11")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r3";

        //STATE 12
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "13";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S12")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 13
        parsingTable[getRowIndex("S13")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r4";

        //STATE 14
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "15";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S14")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 15
        parsingTable[getRowIndex("S15")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r5";

        //STATE 16
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "17";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S16")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 17
        parsingTable[getRowIndex("S17")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r6";

        //STATE 18
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "19";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S18")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 19
        parsingTable[getRowIndex("S19")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r7";

        //STATE 20
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "21";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S20")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 21
        parsingTable[getRowIndex("S21")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r8";

        //STATE 22
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "23";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S22")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 23
        parsingTable[getRowIndex("S23")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r9";

        //STATE 24
        parsingTable[getRowIndex("S24")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s25";

        //STATE 25
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "26";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S25")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 26
        parsingTable[getRowIndex("S26")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r10";

        //STATE 27
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "28";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S27")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 28
        parsingTable[getRowIndex("S28")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r11";

        //STATE 29
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "30";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S29")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 30
        parsingTable[getRowIndex("S30")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r12";

        //STATE 31
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "32";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S31")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 32
        parsingTable[getRowIndex("S32")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r13";

        //STATE 33
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "34";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S33")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 34
        parsingTable[getRowIndex("S34")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r14";

        //STATE 35
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_STATEMENT)] += "36";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_IF_STMT)] += "10";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_WHILE_STMT)] += "12";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_DO_WHILE_STMT)] += "14";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_FOR_STMT)] += "16";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_SWITCH_STMT)] += "18";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_PRINT_STMT)] += "20";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_GET_STMT)] += "22";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s24";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "27";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT)] += "29";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_ARRAY_DECLARE_STMT)] += "31";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "33";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_ARRAY_INIT_STMT)] += "35";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r16";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_IF)] += "s37";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s46";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_DO)] += "s51";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_FOR)] += "s57";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "s66";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "s71";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "s76";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_GET)] += "s81";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "s102";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "121";
        parsingTable[getRowIndex("S35")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //STATE 36
        parsingTable[getRowIndex("S36")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r15";

        //STATE 37
        parsingTable[getRowIndex("S37")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s38";

        //STATE 38
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_BOOLEAN_EXP)] += "39";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_BOOLEAN_OR)] += "181";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_BOOLEAN_AND)] += "182";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "183";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "184";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S38")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s139";

        //STATE 39
        parsingTable[getRowIndex("S39")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "s40";
        parsingTable[getRowIndex("S39")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "s166";

        //STATE 40
        parsingTable[getRowIndex("S40")][getColumnIndex(GrammarRule.TYPE_THEN)] += "s41";

        //STATE 41
        parsingTable[getRowIndex("S41")][getColumnIndex(GrammarRule.VAR_BLOCK)] += "42";
        parsingTable[getRowIndex("S41")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "s7";

        //STATE 42
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.VAR_ELSE_STMT)] += "43";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_ELSE)] += "s44";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_IF)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_DO)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_GET)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r19";
        parsingTable[getRowIndex("S42")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r19";

        //STATE 43
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_IF)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_DO)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_GET)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r17";
        parsingTable[getRowIndex("S43")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r17";

        //STATE 44
        parsingTable[getRowIndex("S44")][getColumnIndex(GrammarRule.VAR_BLOCK)] += "45";
        parsingTable[getRowIndex("S44")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "s7";

        //STATE 45
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_IF)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_DO)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_GET)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r18";
        parsingTable[getRowIndex("S45")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r18";
        
        //state 46
        parsingTable[getRowIndex("S46")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s47";

        //state 47
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_BOOLEAN_EXP)] += "48";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_BOOLEAN_OR)] += "181";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_BOOLEAN_AND)] += "182";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "183";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "184";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S47")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        //state 48
        parsingTable[getRowIndex("S48")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "s49";
        parsingTable[getRowIndex("S48")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "s166";

        //state 49
        parsingTable[getRowIndex("S49")][getColumnIndex(GrammarRule.VAR_BLOCK)] += "50";
        parsingTable[getRowIndex("S49")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "s7";

        //state 50
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_IF)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_DO)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_GET)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r20";
        parsingTable[getRowIndex("S50")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r20";

    }

    public void fill2() {
        //State 51
        parsingTable[getRowIndex("S51")][getColumnIndex(GrammarRule.VAR_BLOCK)] += "52";
        parsingTable[getRowIndex("S51")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "s7";

        //State 52
        parsingTable[getRowIndex("S52")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "s53";

        //State 53
        parsingTable[getRowIndex("S53")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s54";

        //State 54
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_BOOLEAN_EXP)] += "55";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_BOOLEAN_OR)] += "181";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_BOOLEAN_AND)] += "182";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "183";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "184";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S54")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        //State 55
        parsingTable[getRowIndex("S55")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "s56";
        parsingTable[getRowIndex("S55")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "s166";

        //State 56
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_IF)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_DO)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_GET)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r21";
        parsingTable[getRowIndex("S56")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r21";

        //State 57
        parsingTable[getRowIndex("S57")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s58";

        //State 58
        parsingTable[getRowIndex("S58")][getColumnIndex(GrammarRule.VAR_INIT_STMT)] += "59";
        parsingTable[getRowIndex("S58")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "s92";
        parsingTable[getRowIndex("S58")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s98";

        //State 59
        parsingTable[getRowIndex("S59")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "s60";

        //State 60
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_BOOLEAN_EXP)] += "61";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_BOOLEAN_OR)] += "181";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_BOOLEAN_AND)] += "182";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "183";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "184";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S60")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        //State 61
        parsingTable[getRowIndex("S61")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "s62";
        parsingTable[getRowIndex("S61")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "s166";

        //State 62
        parsingTable[getRowIndex("S62")][getColumnIndex(GrammarRule.VAR_INC_STMT)] += "63";
        parsingTable[getRowIndex("S62")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "s84";
        parsingTable[getRowIndex("S62")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "s88";

        //State 63
        parsingTable[getRowIndex("S63")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "s64";

        //State 64
        parsingTable[getRowIndex("S64")][getColumnIndex(GrammarRule.VAR_BLOCK)] += "65";
        parsingTable[getRowIndex("S64")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "s7";

        //State 65
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_IF)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_DO)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_GET)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r22";
        parsingTable[getRowIndex("S65")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r22";

        //State 66
        parsingTable[getRowIndex("S66")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s67";

        //State 67
        parsingTable[getRowIndex("S67")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s68";

        //State 68
        parsingTable[getRowIndex("S68")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "s69";

        //State 69
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.VAR_SWITCH_BLOCK)] += "70";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_WHEN)] += "s140";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_ETC)] += "s146";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_IF)] += "r22";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r23";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_DO)] += "r23";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r23";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r23";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r23";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r23";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_GET)] += "r23";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r23";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r23";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r23";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r23";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r23";
        parsingTable[getRowIndex("S69")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r23";

        //State 70
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_IF)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_DO)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_GET)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r23";
        parsingTable[getRowIndex("S70")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r23";

        //State 71
        parsingTable[getRowIndex("S71")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s72";

        //State 72
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_STRING_CONCAT)] += "73";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_BOOLEAN_EXP)] += "202";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_BOOLEAN_OR)] += "181";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_BOOLEAN_AND)] += "182";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "183";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "184";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S72")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        //State 73
        parsingTable[getRowIndex("S73")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "s74";
        parsingTable[getRowIndex("S73")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "s175";

        //State 74
        parsingTable[getRowIndex("S74")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s75";

        //State 75
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_IF)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_DO)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_GET)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r24";
        parsingTable[getRowIndex("S75")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r24";

        //STATE 76
        parsingTable[getRowIndex("S76")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s77";

        //STATE 77
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_STRING_CONCAT)] += "78";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_BOOLEAN_EXP)] += "202";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_BOOLEAN_OR)] += "181";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_BOOLEAN_AND)] += "182";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "183";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "203";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S77")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        //STATE 78
        parsingTable[getRowIndex("S78")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "s79";
        parsingTable[getRowIndex("S78")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "s175";

        //STATE 79
        parsingTable[getRowIndex("S79")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s80";

        //STATE 80
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_IF)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_DO)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_GET)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r25";
        parsingTable[getRowIndex("S80")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r25";

        //STATE 81
        parsingTable[getRowIndex("S81")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s82";

        //STATE 82
        parsingTable[getRowIndex("S82")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s83";

        //STATE 83
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_IF)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_DO)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_GET)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r26";
        parsingTable[getRowIndex("S83")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r26";

        //STATE 84
        parsingTable[getRowIndex("S84")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "85";
        parsingTable[getRowIndex("S84")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S84")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s151";
        parsingTable[getRowIndex("S84")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "152";

        //STATE 85
        parsingTable[getRowIndex("S85")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "86";
        parsingTable[getRowIndex("S85")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s134";
        parsingTable[getRowIndex("S85")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S85")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S85")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S85")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S85")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S85")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        //STATE 86
        parsingTable[getRowIndex("S86")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s87";
        parsingTable[getRowIndex("S86")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r87";

        //STATE 87
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_IF)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_DO)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_GET)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r27";
        parsingTable[getRowIndex("S87")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r27";

        //STATE 88
        parsingTable[getRowIndex("S88")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "89";
        parsingTable[getRowIndex("S88")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "90";
        parsingTable[getRowIndex("S88")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S88")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S88")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";

        //STATE 89
        parsingTable[getRowIndex("S89")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "90";
        parsingTable[getRowIndex("S89")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S89")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S89")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S89")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S89")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S89")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S89")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        //STATE 90
        parsingTable[getRowIndex("S90")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s91";
        parsingTable[getRowIndex("S90")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r88";

        //STATE 91
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_IF)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_DO)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_GET)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r28";
        parsingTable[getRowIndex("S91")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r28";

        //STATE 92
        parsingTable[getRowIndex("S92")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s93";

        //STATE 93
        parsingTable[getRowIndex("S93")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT_PUMPAH)] += "94";
        parsingTable[getRowIndex("S93")][getColumnIndex(GrammarRule.TYPE_ASSIGNMENT)] += "s95";
        parsingTable[getRowIndex("S93")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "s157";
        parsingTable[getRowIndex("S93")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s160";

        //STATE 94
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_IF)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_DO)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_GET)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r29";
        parsingTable[getRowIndex("S94")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r29";

        //STATE 95
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_STRING_CONCAT)] += "96";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_BOOLEAN_EXP)] += "202";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_BOOLEAN_OR)] += "181";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_BOOLEAN_AND)] += "182";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "183";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "184";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S95")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        //STATE 96
        parsingTable[getRowIndex("S96")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s97";
        parsingTable[getRowIndex("S96")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r85";
        parsingTable[getRowIndex("S96")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "s175";

        //STATE 97
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_IF)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_DO)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_GET)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r32";
        parsingTable[getRowIndex("S97")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r32";

        //STATE 98
        parsingTable[getRowIndex("S98")][getColumnIndex(GrammarRule.TYPE_ASSIGNMENT)] += "s99";

        //STATE 99
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_STRING_CONCAT)] += "100";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_BOOLEAN_EXP)] += "202";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_BOOLEAN_OR)] += "181";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_BOOLEAN_AND)] += "182";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "183";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "184";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S99")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        //STATE 100
        parsingTable[getRowIndex("S100")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s101";
        parsingTable[getRowIndex("S100")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r86";
        parsingTable[getRowIndex("S100")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "s175";

    }

    public void fill3() {
        // state 101
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_IF)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_DO)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_GET)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r33";
        parsingTable[getRowIndex("S101")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r33";

        //state 102
        parsingTable[getRowIndex("S102")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s103";
        parsingTable[getRowIndex("S102")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "112";
        parsingTable[getRowIndex("S102")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S102")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S102")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S102")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S102")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S102")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S102")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";


        //state 103
        parsingTable[getRowIndex("S103")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "s104";

        //state 104
        parsingTable[getRowIndex("S104")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "105";
        parsingTable[getRowIndex("S104")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";


        //state 105
        parsingTable[getRowIndex("S105")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s106";
        parsingTable[getRowIndex("S105")][getColumnIndex(GrammarRule.TYPE_ASSIGNMENT)] += "s107";

        //state 106
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_IF)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_DO)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_GET)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r35";
        parsingTable[getRowIndex("S106")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r35";

        //state 107
        parsingTable[getRowIndex("S107")][getColumnIndex(GrammarRule.TYPE_OPEN_BRACES)] += "s108";

        //state 108
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.VAR_ARRAY_CONTENTS)] += "109";
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "161";
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r40";
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S108")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        //state 109
        parsingTable[getRowIndex("S109")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "s110";

        //state 110
        parsingTable[getRowIndex("S110")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s111";

        //state 111
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_IF)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_DO)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_GET)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r37";
        parsingTable[getRowIndex("S111")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r37";

        //state 112
        parsingTable[getRowIndex("S112")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "s113";

        //state 113
        parsingTable[getRowIndex("S113")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "114";
        parsingTable[getRowIndex("S113")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s127";

        //state 114
        parsingTable[getRowIndex("S114")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s115";
        parsingTable[getRowIndex("S114")][getColumnIndex(GrammarRule.TYPE_ASSIGNMENT)] += "s116";

        //state 115
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_IF)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_DO)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_GET)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r34";
        parsingTable[getRowIndex("S115")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r34";

        //state 116
        parsingTable[getRowIndex("S116")][getColumnIndex(GrammarRule.TYPE_OPEN_BRACES)] += "s117";

        //state 117
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.VAR_ARRAY_CONTENTS)] += "118";
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "161";
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r40";
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S117")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        //state 118
        parsingTable[getRowIndex("S118")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "s119";

        //state 119
        parsingTable[getRowIndex("S119")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s120";

        //state 120
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_IF)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_DO)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_GET)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r36";
        parsingTable[getRowIndex("S120")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r36";

        //state 121
        parsingTable[getRowIndex("S121")][getColumnIndex(GrammarRule.TYPE_ASSIGNMENT)] += "s122";

        //state 122
        parsingTable[getRowIndex("S122")][getColumnIndex(GrammarRule.TYPE_OPEN_BRACES)] += "s123";

        //state 123
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.VAR_ARRAY_CONTENTS)] += "124";
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "161";
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r40";
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S123")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        //state 124
        parsingTable[getRowIndex("S124")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "s125";

        //state 125
        parsingTable[getRowIndex("S125")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s126";

        //state 126
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_IF)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_DO)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_GET)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r38";
        parsingTable[getRowIndex("S126")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r38";

        //State 127
        parsingTable[getRowIndex("S127")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "s128";

        //State 128
        parsingTable[getRowIndex("S128")][getColumnIndex(GrammarRule.TYPE_ASSIGNMENT)] += "r84";
        parsingTable[getRowIndex("S128")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r84";

        //State 129
        parsingTable[getRowIndex("S129")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r69";
        parsingTable[getRowIndex("S129")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r69";
        parsingTable[getRowIndex("S129")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r69";
        parsingTable[getRowIndex("S129")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r69";
        parsingTable[getRowIndex("S129")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r69";
        parsingTable[getRowIndex("S129")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r69";
        parsingTable[getRowIndex("S129")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r69";
        parsingTable[getRowIndex("S129")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r69";
        parsingTable[getRowIndex("S129")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r69";
        parsingTable[getRowIndex("S129")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r69";
        parsingTable[getRowIndex("S129")][getColumnIndex(GrammarRule.TYPE_OPEN_BRACES)] += "r69";
        parsingTable[getRowIndex("S129")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r69";
        parsingTable[getRowIndex("S129")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "r69";

        //State 130
        parsingTable[getRowIndex("S130")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r70";
        parsingTable[getRowIndex("S130")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r70";
        parsingTable[getRowIndex("S130")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r70";
        parsingTable[getRowIndex("S130")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r70";
        parsingTable[getRowIndex("S130")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r70";
        parsingTable[getRowIndex("S130")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r70";
        parsingTable[getRowIndex("S130")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r70";
        parsingTable[getRowIndex("S130")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r70";
        parsingTable[getRowIndex("S130")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r70";
        parsingTable[getRowIndex("S130")][getColumnIndex(GrammarRule.TYPE_OPEN_BRACES)] += "r70";
        parsingTable[getRowIndex("S130")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r70";
        parsingTable[getRowIndex("S130")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r70";
        parsingTable[getRowIndex("S130")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "r70";

        //State 131
        parsingTable[getRowIndex("S131")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r71";
        parsingTable[getRowIndex("S131")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r71";
        parsingTable[getRowIndex("S131")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r71";
        parsingTable[getRowIndex("S131")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r71";
        parsingTable[getRowIndex("S131")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r71";
        parsingTable[getRowIndex("S131")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r71";
        parsingTable[getRowIndex("S131")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r71";
        parsingTable[getRowIndex("S131")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r71";
        parsingTable[getRowIndex("S131")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r71";
        parsingTable[getRowIndex("S131")][getColumnIndex(GrammarRule.TYPE_OPEN_BRACES)] += "r71";
        parsingTable[getRowIndex("S131")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r71";
        parsingTable[getRowIndex("S131")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r71";
        parsingTable[getRowIndex("S131")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "r71";

        //State 132
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r72";
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r72";
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r72";
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r72";
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r72";
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r72";
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r72";
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r72";
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r72";
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_OPEN_BRACES)] += "r72";
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r72";
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r72";
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "r72";
        parsingTable[getRowIndex("S132")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r72";

        //State 133
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r73";
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r73";
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r73";
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r73";
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r73";
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r73";
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r73";
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r73";
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r73";
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r73";
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r73";
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_OPEN_BRACES)] += "r73";
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r73";
        parsingTable[getRowIndex("S133")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "r73";

        //State 134
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r74";
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r74";
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r74";
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r74";
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r74";
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r74";
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r74";
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r74";
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r74";
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r74";
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r74";
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "r74";
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r74";
        parsingTable[getRowIndex("S134")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r74";

        //State 135
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r75";
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r75";
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r75";
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r75";
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r75";
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r75";
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r75";
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r75";
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r75";
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r75";
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r75";
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "r75";
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r75";
        parsingTable[getRowIndex("S135")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r75";

        //State 136
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r76";
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r76";
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r76";
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r76";
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r76";
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r76";
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r76";
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r76";
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r76";
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r76";
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r76";
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "r76";
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r76";
        parsingTable[getRowIndex("S136")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r76";

        //State 137
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r77";
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r77";
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r77";
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r77";
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r77";
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r77";
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r77";
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r77";
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r77";
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r77";
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r77";
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "r77";
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r77";
        parsingTable[getRowIndex("S137")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r77";

        //State 138
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r78";
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r78";
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r78";
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r78";
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r78";
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r78";
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r78";
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r78";
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r78";
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r78";
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r78";
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "r78";
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r78";
        parsingTable[getRowIndex("S138")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r78";

        //State 139

        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r79";
        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r79";
        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r79";
        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r79";
        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r79";
        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r79";
        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r79";
        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r79";
        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r79";
        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r79";
        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r79";
        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "r79";
        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r79";
        parsingTable[getRowIndex("S139")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "r79";

        //State 140
        parsingTable[getRowIndex("S140")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "141";
        parsingTable[getRowIndex("S140")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S140")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S140")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S140")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S140")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S140")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S140")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S140")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S140")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S140")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S140")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        //State 141
        parsingTable[getRowIndex("S141")][getColumnIndex(GrammarRule.VAR_BLOCK)] += "142";
        parsingTable[getRowIndex("S141")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "s7";

        //State 142
        parsingTable[getRowIndex("S142")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s143";

        //State 143
        parsingTable[getRowIndex("S143")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s144";

        //State 144
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.VAR_SWITCH_BLOCK)] += "145";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_WHEN)] += "s140";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_ETC)] += "s146";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_IF)] += "r45";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r45";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_DO)] += "r45";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r45";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r45";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r45";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r45";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_GET)] += "r45";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r45";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r45";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r45";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r45";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r45";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r45";
        parsingTable[getRowIndex("S144")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r45";

        //State 145
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_IF)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_DO)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_GET)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r43";
        parsingTable[getRowIndex("S145")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r43";

        //State 146
        parsingTable[getRowIndex("S146")][getColumnIndex(GrammarRule.VAR_BLOCK)] += "147";
        parsingTable[getRowIndex("S146")][getColumnIndex(GrammarRule.TYPE_OPEN_BLOCK)] += "s7";

        //State 147
        parsingTable[getRowIndex("S147")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "s148";

        //State 148
        parsingTable[getRowIndex("S148")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s149";

        //State 149
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_IF)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_DO)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_GET)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r44";
        parsingTable[getRowIndex("S149")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r44";

        //State 150
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r80";
        parsingTable[getRowIndex("S150")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r80";

    }

    public void fill4() {


        // state 151
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r81";
        parsingTable[getRowIndex("S151")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r81";

        // state 152
        parsingTable[getRowIndex("S152")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s153";
        parsingTable[getRowIndex("S152")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "155";
        parsingTable[getRowIndex("S152")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S152")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S152")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S152")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S152")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S152")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S152")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        // state 153
        parsingTable[getRowIndex("S153")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "s154";

        // state 154
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r83";
        parsingTable[getRowIndex("S154")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r83";

        // state 155
        parsingTable[getRowIndex("S155")][getColumnIndex(GrammarRule.TYPE_ARRAY_CLOSING)] += "s156";

        // state 156
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r82";
        parsingTable[getRowIndex("S156")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r82";

        // state 157
        parsingTable[getRowIndex("S157")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s158";

        // state 158
        parsingTable[getRowIndex("S158")][getColumnIndex(GrammarRule.VAR_DECLARE_STMT_PUMPAH)] += "159";
        parsingTable[getRowIndex("S158")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "s157";
        parsingTable[getRowIndex("S158")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "s160";

        // state 159
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_IF)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_DO)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_GET)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r30";
        parsingTable[getRowIndex("S159")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r30";

        // state 160
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_IF)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_WHILE)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_DO)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_FOR)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_SWITCH)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_OUTPUT)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_OUTPUT_2)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_GET)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_INC)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT_DEC)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_DATA_TYPE)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_ARRAY_DATA_TYPE)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_CLOSE_BLOCK)] += "r31";
        parsingTable[getRowIndex("S160")][getColumnIndex(GrammarRule.TYPE_BREAK)] += "r31";

        // state 161
        parsingTable[getRowIndex("S161")][getColumnIndex(GrammarRule.VAR_ARRAY_CONTENTS_PUMPAH)] += "162";
        parsingTable[getRowIndex("S161")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "s163";
        parsingTable[getRowIndex("S161")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r42";

        // state 162
        parsingTable[getRowIndex("S162")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r39";

        // state 163
        parsingTable[getRowIndex("S163")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "164";
        parsingTable[getRowIndex("S163")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S163")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S163")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S163")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S163")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S163")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S163")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S163")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S163")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S163")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S163")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        // state 164
        parsingTable[getRowIndex("S164")][getColumnIndex(GrammarRule.VAR_ARRAY_CONTENTS_PUMPAH)] += "165";
        parsingTable[getRowIndex("S164")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "s163";
        parsingTable[getRowIndex("S164")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r42";

        // state 165
        parsingTable[getRowIndex("S165")][getColumnIndex(GrammarRule.TYPE_CLOSE_BRACES)] += "r41";

        // state 166
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.VAR_BOOLEAN_OR)] += "167";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.VAR_BOOLEAN_AND)] += "182";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "183";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "184";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S166")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s189";

        // state 167
        parsingTable[getRowIndex("S167")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r59";
        parsingTable[getRowIndex("S167")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r59";
        parsingTable[getRowIndex("S167")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r59";
        parsingTable[getRowIndex("S167")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r59";
        parsingTable[getRowIndex("S167")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r59";
        parsingTable[getRowIndex("S167")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "s168";

        // state 168
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.VAR_BOOLEAN_AND)] += "169";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "183";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "184";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S168")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s189";

        // state 169
        parsingTable[getRowIndex("S169")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r61";
        parsingTable[getRowIndex("S169")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r61";
        parsingTable[getRowIndex("S169")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r61";
        parsingTable[getRowIndex("S169")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r61";
        parsingTable[getRowIndex("S169")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r61";
        parsingTable[getRowIndex("S169")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r61";
        parsingTable[getRowIndex("S169")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "s170";

        // state 170
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "171";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "184";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S170")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s189";

        // state 171
        parsingTable[getRowIndex("S171")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r63";
        parsingTable[getRowIndex("S171")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r63";
        parsingTable[getRowIndex("S171")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r63";
        parsingTable[getRowIndex("S171")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r63";
        parsingTable[getRowIndex("S171")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r63";
        parsingTable[getRowIndex("S171")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r63";
        parsingTable[getRowIndex("S171")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r63";

        // state 172
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "173";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "184";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S172")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s189";

        // state 173
        parsingTable[getRowIndex("S173")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r65";
        parsingTable[getRowIndex("S173")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r65";
        parsingTable[getRowIndex("S173")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r65";
        parsingTable[getRowIndex("S173")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r65";
        parsingTable[getRowIndex("S173")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r65";
        parsingTable[getRowIndex("S173")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r65";
        parsingTable[getRowIndex("S173")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r65";

        // state 174
        parsingTable[getRowIndex("S174")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r66";
        parsingTable[getRowIndex("S174")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r66";
        parsingTable[getRowIndex("S174")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r66";
        parsingTable[getRowIndex("S174")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r66";
        parsingTable[getRowIndex("S174")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r66";
        parsingTable[getRowIndex("S174")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r66";
        parsingTable[getRowIndex("S174")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r66";

        // state 175
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_STRING_CONCAT)] += "176";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_BOOLEAN_EXP)] += "202";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_BOOLEAN_OR)] += "181";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_BOOLEAN_AND)] += "182";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "183";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "184";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S175")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s189";

        // state 176
        parsingTable[getRowIndex("S176")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r57";
        parsingTable[getRowIndex("S176")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r57";
        parsingTable[getRowIndex("S176")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r57";
        parsingTable[getRowIndex("S176")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r57s175";   // shift reduce problem

        // state 177

        // state 178
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_STRING_CONCAT)] += "200";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_BOOLEAN_EXP)] += "202";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "184";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_BOOLEAN_OR)] += "181";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_BOOLEAN_AND)] += "182";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_BOOLEAN_LGE)] += "183";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_NOT)] += "s172";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_BOOLEAN_NOT)] += "174";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S178")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        // state 179
        parsingTable[getRowIndex("S179")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "s180";
        parsingTable[getRowIndex("S179")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "s166";

        // state 180
        parsingTable[getRowIndex("S180")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r68";
        parsingTable[getRowIndex("S180")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r68";
        parsingTable[getRowIndex("S180")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r68";
        parsingTable[getRowIndex("S180")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r68";
        parsingTable[getRowIndex("S180")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r68";
        parsingTable[getRowIndex("S180")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r68";
        parsingTable[getRowIndex("S180")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r68";

        // state 181
        parsingTable[getRowIndex("S181")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r60";
        parsingTable[getRowIndex("S181")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r60";
        parsingTable[getRowIndex("S181")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r60";
        parsingTable[getRowIndex("S181")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r60";
        parsingTable[getRowIndex("S181")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r60";
        parsingTable[getRowIndex("S181")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "s168";

        // state 182
        parsingTable[getRowIndex("S182")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r62";
        parsingTable[getRowIndex("S182")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r62";
        parsingTable[getRowIndex("S182")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r62";
        parsingTable[getRowIndex("S182")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r62";
        parsingTable[getRowIndex("S182")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r62";
        parsingTable[getRowIndex("S182")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r62";
        parsingTable[getRowIndex("S182")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "s170";

        // state 183
        parsingTable[getRowIndex("S183")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r64";
        parsingTable[getRowIndex("S183")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r64";
        parsingTable[getRowIndex("S183")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r64";
        parsingTable[getRowIndex("S183")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r64";
        parsingTable[getRowIndex("S183")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r64";
        parsingTable[getRowIndex("S183")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r64";
        parsingTable[getRowIndex("S183")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r64";

        // state 184
        parsingTable[getRowIndex("S184")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "s185";
        parsingTable[getRowIndex("S184")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r67";
        parsingTable[getRowIndex("S184")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r67";
        parsingTable[getRowIndex("S184")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r67";
        parsingTable[getRowIndex("S184")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r67";
        parsingTable[getRowIndex("S184")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r67";
        parsingTable[getRowIndex("S184")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r67";
        parsingTable[getRowIndex("S184")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r67";

        // state 185
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "186";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S185")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        // state 186
        parsingTable[getRowIndex("S186")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r46";
        parsingTable[getRowIndex("S186")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r46";
        parsingTable[getRowIndex("S186")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r46";
        parsingTable[getRowIndex("S186")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r46";
        parsingTable[getRowIndex("S186")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r46";
        parsingTable[getRowIndex("S186")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r46";
        parsingTable[getRowIndex("S186")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r46";
        parsingTable[getRowIndex("S186")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r46";
        parsingTable[getRowIndex("S186")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "s187";

        // state 187
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "188";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S187")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        // state 188
        parsingTable[getRowIndex("S188")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r48";
        parsingTable[getRowIndex("S188")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r48";
        parsingTable[getRowIndex("S188")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r48";
        parsingTable[getRowIndex("S188")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r48";
        parsingTable[getRowIndex("S188")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r48";
        parsingTable[getRowIndex("S188")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r48";
        parsingTable[getRowIndex("S188")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r48";
        parsingTable[getRowIndex("S188")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r48";
        parsingTable[getRowIndex("S188")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r48";

        // state 189
        parsingTable[getRowIndex("S189")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "s190"; // shift reduce problem
        parsingTable[getRowIndex("S189")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r51";
        parsingTable[getRowIndex("S189")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r51";
        parsingTable[getRowIndex("S189")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r51";
        parsingTable[getRowIndex("S189")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r51";
        parsingTable[getRowIndex("S189")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r51";
        parsingTable[getRowIndex("S189")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r51";
        parsingTable[getRowIndex("S189")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r51";
        parsingTable[getRowIndex("S189")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r51";
        parsingTable[getRowIndex("S189")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r51";

        // state 190
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "191";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S190")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        // state 191
        parsingTable[getRowIndex("S191")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r50";
        parsingTable[getRowIndex("S191")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r50";
        parsingTable[getRowIndex("S191")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r50";
        parsingTable[getRowIndex("S191")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r50";
        parsingTable[getRowIndex("S191")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r50";
        parsingTable[getRowIndex("S191")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r50";
        parsingTable[getRowIndex("S191")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r50";
        parsingTable[getRowIndex("S191")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r50";
        parsingTable[getRowIndex("S191")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r50";

        // state 192
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "193";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "s178";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S192")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        // state 193
        parsingTable[getRowIndex("S193")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r52";
        parsingTable[getRowIndex("S193")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r52";
        parsingTable[getRowIndex("S193")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r52";
        parsingTable[getRowIndex("S193")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r52";
        parsingTable[getRowIndex("S193")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r52";
        parsingTable[getRowIndex("S193")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r52";
        parsingTable[getRowIndex("S193")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r52";
        parsingTable[getRowIndex("S193")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r52";
        parsingTable[getRowIndex("S193")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r52";
        parsingTable[getRowIndex("S193")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r52";

        // state 194
        parsingTable[getRowIndex("S194")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r53";
        parsingTable[getRowIndex("S194")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r53";
        parsingTable[getRowIndex("S194")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r53";
        parsingTable[getRowIndex("S194")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r53";
        parsingTable[getRowIndex("S194")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r53";
        parsingTable[getRowIndex("S194")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r53";
        parsingTable[getRowIndex("S194")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r53";
        parsingTable[getRowIndex("S194")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r53";
        parsingTable[getRowIndex("S194")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r53";
        parsingTable[getRowIndex("S194")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r53";

        // state 195
        parsingTable[getRowIndex("S195")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r47";
        parsingTable[getRowIndex("S195")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r47";
        parsingTable[getRowIndex("S195")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r47";
        parsingTable[getRowIndex("S195")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r47";
        parsingTable[getRowIndex("S195")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r47";
        parsingTable[getRowIndex("S195")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r47";
        parsingTable[getRowIndex("S195")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r47";
        parsingTable[getRowIndex("S195")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r47";
        parsingTable[getRowIndex("S195")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "s187";

        // state 196
        parsingTable[getRowIndex("S196")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r49";
        parsingTable[getRowIndex("S196")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r49";
        parsingTable[getRowIndex("S196")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r49";
        parsingTable[getRowIndex("S196")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r49";
        parsingTable[getRowIndex("S196")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r49";
        parsingTable[getRowIndex("S196")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r49";
        parsingTable[getRowIndex("S196")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r49";
        parsingTable[getRowIndex("S196")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r49";
        parsingTable[getRowIndex("S196")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r49";

        // state 197
        parsingTable[getRowIndex("S197")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r54";
        parsingTable[getRowIndex("S197")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r54";
        parsingTable[getRowIndex("S197")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r54";
        parsingTable[getRowIndex("S197")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r54";
        parsingTable[getRowIndex("S197")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r54";
        parsingTable[getRowIndex("S197")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r54";
        parsingTable[getRowIndex("S197")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r54";
        parsingTable[getRowIndex("S197")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r54";
        parsingTable[getRowIndex("S197")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r54";
        parsingTable[getRowIndex("S197")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r54";

        // state 198
        parsingTable[getRowIndex("S198")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r55";
        parsingTable[getRowIndex("S198")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r55";
        parsingTable[getRowIndex("S198")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r55";
        parsingTable[getRowIndex("S198")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r55";
        parsingTable[getRowIndex("S198")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r55";
        parsingTable[getRowIndex("S198")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r55";
        parsingTable[getRowIndex("S198")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r55";
        parsingTable[getRowIndex("S198")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r55";
        parsingTable[getRowIndex("S198")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r55";
        parsingTable[getRowIndex("S198")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r55";

        // state 199
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.VAR_MATH_EXP)] += "200";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.VAR_MATH_TERM)] += "195";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.VAR_MATH_FACTOR)] += "196";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.VAR_MATH_POWER)] += "189";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_TO_DATA_TYPE)] += "s192";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.VAR_MATH_CAST)] += "194";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.VAR_IDENTIFIER)] += "197";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.VAR_CONSTANT)] += "198";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_OPEN_PARENTHESIS)] += "S199";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_IDENTIFIER)] += "s150";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.VAR_ARRAY_ID)] += "151";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_ARRAY_IDENTIFIER)] += "s152";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_LITERAL)] += "s129";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_TRUE)] += "s130";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_FALSE)] += "s131";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.VAR_NUMERIC_CONST)] += "132";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_MAYBE_OCT)] += "s133";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_MAYBE_BIN)] += "s134";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_NUMERIC_FLOAT)] += "s135";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_NUMERIC)] += "s136";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_HEX_CONSTANT)] += "s137";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_OCT_CONSTANT)] += "s138";
        parsingTable[getRowIndex("S199")][getColumnIndex(GrammarRule.TYPE_BIN_CONSTANT)] += "s139";

        // state 200
        parsingTable[getRowIndex("S200")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "s201";
        parsingTable[getRowIndex("S200")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "s175";

        // state 201
        parsingTable[getRowIndex("S201")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r56";
        parsingTable[getRowIndex("S201")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r56";
        parsingTable[getRowIndex("S201")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r56";
        parsingTable[getRowIndex("S201")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r56";
        parsingTable[getRowIndex("S201")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r56";
        parsingTable[getRowIndex("S201")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r56";
        parsingTable[getRowIndex("S201")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r56";
        parsingTable[getRowIndex("S201")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "r56";
        parsingTable[getRowIndex("S201")][getColumnIndex(GrammarRule.TYPE_MULTIPLY_DIVIDE)] += "r56";
        parsingTable[getRowIndex("S201")][getColumnIndex(GrammarRule.TYPE_EXPONENTIAL)] += "r56";

        // state 202
        parsingTable[getRowIndex("S202")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r58";
        parsingTable[getRowIndex("S202")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r58";
        parsingTable[getRowIndex("S202")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r58";
        parsingTable[getRowIndex("S202")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r58";
        parsingTable[getRowIndex("S202")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "s166";

        // state 203
        parsingTable[getRowIndex("S203")][getColumnIndex(GrammarRule.TYPE_CLOSE_PARENTHESIS)] += "r67";
        parsingTable[getRowIndex("S203")][getColumnIndex(GrammarRule.TYPE_DELIMITER)] += "r67";
        parsingTable[getRowIndex("S203")][getColumnIndex(GrammarRule.TYPE_COMMA)] += "r67";
        parsingTable[getRowIndex("S203")][getColumnIndex(GrammarRule.TYPE_STRING_CONCAT)] += "r67";
        parsingTable[getRowIndex("S203")][getColumnIndex(GrammarRule.TYPE_OR_NOR)] += "r67";
        parsingTable[getRowIndex("S203")][getColumnIndex(GrammarRule.TYPE_AND_NAND)] += "r67";
        parsingTable[getRowIndex("S203")][getColumnIndex(GrammarRule.TYPE_RELATIONAL)] += "r67";
        parsingTable[getRowIndex("S203")][getColumnIndex(GrammarRule.TYPE_ADD_SUBTRACT)] += "s185";
    }

    public GrammarRule[] cfgRules(String s) {
        GrammarRule[] rules = {
            new GrammarRule(s,
            new String[]{Token.TYPE_PROGNAME, Token.TYPE_PROGRAM_ID, Token.TYPE_OPEN_PROGRAM_BLOCK,
                GrammarRule.VAR_BLOCK, Token.TYPE_CLOSE_PROGRAM_BLOCK}),
            new GrammarRule(GrammarRule.VAR_BLOCK,
            new String[]{Token.TYPE_OPEN_BLOCK, GrammarRule.VAR_STATEMENT, Token.TYPE_CLOSE_BLOCK}),
            new GrammarRule(GrammarRule.VAR_STATEMENT,
            new String[]{GrammarRule.VAR_IF_STMT, GrammarRule.VAR_STATEMENT}),
            new GrammarRule(GrammarRule.VAR_STATEMENT,
            new String[]{GrammarRule.VAR_WHILE_STMT, GrammarRule.VAR_STATEMENT}),
            new GrammarRule(GrammarRule.VAR_STATEMENT,
            new String[]{GrammarRule.VAR_DO_WHILE_STMT, GrammarRule.VAR_STATEMENT}),
            new GrammarRule(GrammarRule.VAR_STATEMENT,
            new String[]{GrammarRule.VAR_FOR_STMT, GrammarRule.VAR_STATEMENT}),
            new GrammarRule(GrammarRule.VAR_STATEMENT,
            new String[]{GrammarRule.VAR_SWITCH_STMT, GrammarRule.VAR_STATEMENT}),
            new GrammarRule(GrammarRule.VAR_STATEMENT,
            new String[]{GrammarRule.VAR_PRINT_STMT, GrammarRule.VAR_STATEMENT}),
            new GrammarRule(GrammarRule.VAR_STATEMENT,
            new String[]{GrammarRule.VAR_GET_STMT, GrammarRule.VAR_STATEMENT}),
            new GrammarRule(GrammarRule.VAR_STATEMENT,
            new String[]{Token.TYPE_BREAK, Token.TYPE_DELIMITER, GrammarRule.VAR_STATEMENT}),
            new GrammarRule(GrammarRule.VAR_STATEMENT,
            new String[]{GrammarRule.VAR_INC_STMT, GrammarRule.VAR_STATEMENT}),
            new GrammarRule(GrammarRule.VAR_STATEMENT,
            new String[]{GrammarRule.VAR_DECLARE_STMT, GrammarRule.VAR_STATEMENT}),
            new GrammarRule(GrammarRule.VAR_STATEMENT,
            new String[]{GrammarRule.VAR_ARRAY_DECLARE_STMT, GrammarRule.VAR_STATEMENT}),
            new GrammarRule(GrammarRule.VAR_STATEMENT,
            new String[]{GrammarRule.VAR_INIT_STMT, GrammarRule.VAR_STATEMENT}),
            new GrammarRule(GrammarRule.VAR_STATEMENT,
            new String[]{GrammarRule.VAR_ARRAY_INIT_STMT, GrammarRule.VAR_STATEMENT}),
            new GrammarRule(GrammarRule.VAR_STATEMENT, new String[]{""}),
            new GrammarRule(GrammarRule.VAR_IF_STMT,
            new String[]{Token.TYPE_IF, Token.TYPE_OPEN_PARENTHESIS, GrammarRule.VAR_BOOLEAN_EXP,
                Token.TYPE_CLOSE_PARENTHESIS, Token.TYPE_THEN, GrammarRule.VAR_BLOCK,
                GrammarRule.VAR_ELSE_STMT}),
            new GrammarRule(GrammarRule.VAR_ELSE_STMT,
            new String[]{Token.TYPE_ELSE, GrammarRule.VAR_BLOCK}),
            new GrammarRule(GrammarRule.VAR_ELSE_STMT,
            new String[]{""}),
            new GrammarRule(GrammarRule.VAR_WHILE_STMT,
            new String[]{Token.TYPE_WHILE, Token.TYPE_OPEN_PARENTHESIS, GrammarRule.VAR_BOOLEAN_EXP,
                Token.TYPE_CLOSE_PARENTHESIS, GrammarRule.VAR_BLOCK}),
            new GrammarRule(GrammarRule.VAR_DO_WHILE_STMT,
            new String[]{Token.TYPE_DO, GrammarRule.VAR_BLOCK, Token.TYPE_WHILE,
                Token.TYPE_OPEN_PARENTHESIS, GrammarRule.VAR_BOOLEAN_EXP, Token.TYPE_CLOSE_PARENTHESIS}),
            new GrammarRule(GrammarRule.VAR_FOR_STMT,
            new String[]{Token.TYPE_FOR, Token.TYPE_OPEN_PARENTHESIS, GrammarRule.VAR_INIT_STMT, Token.TYPE_COMMA,
                GrammarRule.VAR_BOOLEAN_EXP, Token.TYPE_COMMA, GrammarRule.VAR_INC_STMT,
                Token.TYPE_CLOSE_PARENTHESIS, GrammarRule.VAR_BLOCK}),
            new GrammarRule(GrammarRule.VAR_SWITCH_STMT,
            new String[]{Token.TYPE_SWITCH, Token.TYPE_OPEN_PARENTHESIS, Token.TYPE_IDENTIFIER,
                Token.TYPE_CLOSE_PARENTHESIS, GrammarRule.VAR_SWITCH_BLOCK}),
            new GrammarRule(GrammarRule.VAR_PRINT_STMT,
            new String[]{Token.TYPE_OUTPUT, Token.TYPE_OPEN_PARENTHESIS, GrammarRule.VAR_STRING_CONCAT,
                Token.TYPE_CLOSE_PARENTHESIS, Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_PRINT_STMT,
            new String[]{Token.TYPE_OUTPUT_2, Token.TYPE_OPEN_PARENTHESIS, GrammarRule.VAR_STRING_CONCAT,
                Token.TYPE_CLOSE_PARENTHESIS, Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_GET_STMT,
            new String[]{Token.TYPE_GET, Token.TYPE_IDENTIFIER, Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_INC_STMT,
            new String[]{Token.TYPE_ADD_SUBTRACT_INC, GrammarRule.VAR_IDENTIFIER, GrammarRule.VAR_NUMERIC_CONST,
                Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_INC_STMT,
            new String[]{Token.TYPE_ADD_SUBTRACT_DEC, GrammarRule.VAR_IDENTIFIER, GrammarRule.VAR_NUMERIC_CONST,
                Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_DECLARE_STMT,
            new String[]{Token.TYPE_DATA_TYPE, Token.TYPE_IDENTIFIER, GrammarRule.VAR_DECLARE_STMT_PUMPAH}),
            new GrammarRule(GrammarRule.VAR_DECLARE_STMT_PUMPAH,
            new String[]{Token.TYPE_COMMA, Token.TYPE_IDENTIFIER, GrammarRule.VAR_DECLARE_STMT_PUMPAH}),
            new GrammarRule(GrammarRule.VAR_DECLARE_STMT_PUMPAH,
            new String[]{Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_INIT_STMT,
            new String[]{Token.TYPE_DATA_TYPE, Token.TYPE_IDENTIFIER, Token.TYPE_ASSIGNMENT, GrammarRule.VAR_STRING_CONCAT,
                Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_INIT_STMT,
            new String[]{Token.TYPE_IDENTIFIER, Token.TYPE_ASSIGNMENT, GrammarRule.VAR_STRING_CONCAT,
                Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_ARRAY_DECLARE_STMT,
            new String[]{Token.TYPE_ARRAY_DATA_TYPE, GrammarRule.VAR_NUMERIC_CONST, Token.TYPE_ARRAY_CLOSING,
                GrammarRule.VAR_ARRAY_ID, Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_ARRAY_DECLARE_STMT,
            new String[]{Token.TYPE_ARRAY_DATA_TYPE, Token.TYPE_IDENTIFIER, Token.TYPE_ARRAY_CLOSING,
                GrammarRule.VAR_ARRAY_ID, Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_ARRAY_INIT_STMT,
            new String[]{Token.TYPE_ARRAY_DATA_TYPE, GrammarRule.VAR_NUMERIC_CONST, Token.TYPE_ARRAY_CLOSING,
                GrammarRule.VAR_ARRAY_ID, Token.TYPE_ASSIGNMENT, Token.TYPE_OPEN_BRACES,
                GrammarRule.VAR_ARRAY_CONTENTS, Token.TYPE_CLOSE_BRACES, Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_ARRAY_INIT_STMT,
            new String[]{Token.TYPE_ARRAY_DATA_TYPE, Token.TYPE_IDENTIFIER, Token.TYPE_ARRAY_CLOSING,
                GrammarRule.VAR_ARRAY_ID, Token.TYPE_ASSIGNMENT, Token.TYPE_OPEN_BRACES,
                GrammarRule.VAR_ARRAY_CONTENTS, Token.TYPE_CLOSE_BRACES, Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_ARRAY_INIT_STMT,
            new String[]{GrammarRule.VAR_ARRAY_ID, Token.TYPE_ASSIGNMENT,
                Token.TYPE_OPEN_BRACES, GrammarRule.VAR_ARRAY_CONTENTS, Token.TYPE_CLOSE_BRACES, Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_ARRAY_CONTENTS,
            new String[]{GrammarRule.VAR_CONSTANT, GrammarRule.VAR_ARRAY_CONTENTS_PUMPAH}),
            new GrammarRule(GrammarRule.VAR_ARRAY_CONTENTS, new String[]{""}),
            new GrammarRule(GrammarRule.VAR_ARRAY_CONTENTS_PUMPAH,
            new String[]{Token.TYPE_COMMA, GrammarRule.VAR_CONSTANT, GrammarRule.VAR_ARRAY_CONTENTS_PUMPAH}),
            new GrammarRule(GrammarRule.VAR_ARRAY_CONTENTS_PUMPAH,
            new String[]{""}),
            new GrammarRule(GrammarRule.VAR_SWITCH_BLOCK,
            new String[]{Token.TYPE_WHEN, GrammarRule.VAR_CONSTANT, GrammarRule.VAR_BLOCK,
                Token.TYPE_BREAK, Token.TYPE_DELIMITER, GrammarRule.VAR_SWITCH_BLOCK}),
            new GrammarRule(GrammarRule.VAR_SWITCH_BLOCK,
            new String[]{Token.TYPE_ETC, GrammarRule.VAR_BLOCK, Token.TYPE_BREAK,
                Token.TYPE_DELIMITER}),
            new GrammarRule(GrammarRule.VAR_SWITCH_BLOCK, new String[]{""}),
            new GrammarRule(GrammarRule.VAR_MATH_EXP,
            new String[]{GrammarRule.VAR_MATH_EXP, Token.TYPE_ADD_SUBTRACT, GrammarRule.VAR_MATH_TERM}),
            new GrammarRule(GrammarRule.VAR_MATH_EXP,
            new String[]{GrammarRule.VAR_MATH_TERM}),
            new GrammarRule(GrammarRule.VAR_MATH_TERM,
            new String[]{GrammarRule.VAR_MATH_TERM, Token.TYPE_MULTIPLY_DIVIDE, GrammarRule.VAR_MATH_FACTOR}),
            new GrammarRule(GrammarRule.VAR_MATH_TERM,
            new String[]{GrammarRule.VAR_MATH_FACTOR}),
            new GrammarRule(GrammarRule.VAR_MATH_FACTOR,
            new String[]{GrammarRule.VAR_MATH_POWER, Token.TYPE_EXPONENTIAL, GrammarRule.VAR_MATH_FACTOR}),
            new GrammarRule(GrammarRule.VAR_MATH_FACTOR,
            new String[]{GrammarRule.VAR_MATH_POWER}),
            new GrammarRule(GrammarRule.VAR_MATH_POWER,
            new String[]{Token.TYPE_TO_DATA_TYPE, GrammarRule.VAR_MATH_CAST}),
            new GrammarRule(GrammarRule.VAR_MATH_POWER,
            new String[]{GrammarRule.VAR_MATH_CAST}),
            new GrammarRule(GrammarRule.VAR_MATH_CAST,
            new String[]{GrammarRule.VAR_IDENTIFIER}),
            new GrammarRule(GrammarRule.VAR_MATH_CAST,
            new String[]{GrammarRule.VAR_CONSTANT}),
            new GrammarRule(GrammarRule.VAR_MATH_CAST,
            new String[]{Token.TYPE_OPEN_PARENTHESIS, GrammarRule.VAR_STRING_CONCAT, Token.TYPE_CLOSE_PARENTHESIS}),
            new GrammarRule(GrammarRule.VAR_STRING_CONCAT,
            new String[]{GrammarRule.VAR_STRING_CONCAT, Token.TYPE_STRING_CONCAT, GrammarRule.VAR_STRING_CONCAT}),
            new GrammarRule(GrammarRule.VAR_STRING_CONCAT,
            new String[]{GrammarRule.VAR_BOOLEAN_EXP}),
            new GrammarRule(GrammarRule.VAR_BOOLEAN_EXP,
            new String[]{GrammarRule.VAR_BOOLEAN_EXP, Token.TYPE_OR_NOR, GrammarRule.VAR_BOOLEAN_OR}),
            new GrammarRule(GrammarRule.VAR_BOOLEAN_EXP,
            new String[]{GrammarRule.VAR_BOOLEAN_OR}),
            new GrammarRule(GrammarRule.VAR_BOOLEAN_OR,
            new String[]{GrammarRule.VAR_BOOLEAN_OR, Token.TYPE_AND_NAND, GrammarRule.VAR_BOOLEAN_AND}),
            new GrammarRule(GrammarRule.VAR_BOOLEAN_OR,
            new String[]{GrammarRule.VAR_BOOLEAN_AND}),
            new GrammarRule(GrammarRule.VAR_BOOLEAN_AND,
            new String[]{GrammarRule.VAR_BOOLEAN_AND, Token.TYPE_RELATIONAL, GrammarRule.VAR_BOOLEAN_LGE}),
            new GrammarRule(GrammarRule.VAR_BOOLEAN_AND,
            new String[]{GrammarRule.VAR_BOOLEAN_LGE}),
            new GrammarRule(GrammarRule.VAR_BOOLEAN_LGE,
            new String[]{Token.TYPE_NOT, GrammarRule.VAR_BOOLEAN_LGE}),
            new GrammarRule(GrammarRule.VAR_BOOLEAN_LGE,
            new String[]{GrammarRule.VAR_BOOLEAN_NOT}),
            new GrammarRule(GrammarRule.VAR_BOOLEAN_NOT,
            new String[]{GrammarRule.VAR_MATH_EXP}),
            new GrammarRule(GrammarRule.VAR_BOOLEAN_NOT,
            new String[]{Token.TYPE_OPEN_PARENTHESIS, GrammarRule.VAR_BOOLEAN_EXP, Token.TYPE_CLOSE_PARENTHESIS}),
            new GrammarRule(GrammarRule.VAR_CONSTANT, new String[]{Token.TYPE_LITERAL}),
            new GrammarRule(GrammarRule.VAR_CONSTANT, new String[]{Token.TYPE_TRUE}),
            new GrammarRule(GrammarRule.VAR_CONSTANT, new String[]{Token.TYPE_FALSE}),
            new GrammarRule(GrammarRule.VAR_CONSTANT, new String[]{GrammarRule.VAR_NUMERIC_CONST}),
            new GrammarRule(GrammarRule.VAR_NUMERIC_CONST, new String[]{Token.TYPE_MAYBE_OCT}),
            new GrammarRule(GrammarRule.VAR_NUMERIC_CONST, new String[]{Token.TYPE_MAYBE_BIN}),
            new GrammarRule(GrammarRule.VAR_NUMERIC_CONST, new String[]{Token.TYPE_NUMERIC_FLOAT}),
            new GrammarRule(GrammarRule.VAR_NUMERIC_CONST, new String[]{Token.TYPE_NUMERIC}),
            new GrammarRule(GrammarRule.VAR_NUMERIC_CONST, new String[]{Token.TYPE_HEX_CONSTANT}),
            new GrammarRule(GrammarRule.VAR_NUMERIC_CONST, new String[]{Token.TYPE_OCT_CONSTANT}),
            new GrammarRule(GrammarRule.VAR_NUMERIC_CONST, new String[]{Token.TYPE_BIN_CONSTANT}),
            new GrammarRule(GrammarRule.VAR_IDENTIFIER, new String[]{Token.TYPE_IDENTIFIER}),
            new GrammarRule(GrammarRule.VAR_IDENTIFIER, new String[]{GrammarRule.VAR_ARRAY_ID}),
            new GrammarRule(GrammarRule.VAR_ARRAY_ID,
            new String[]{Token.TYPE_ARRAY_IDENTIFIER, GrammarRule.VAR_NUMERIC_CONST, Token.TYPE_ARRAY_CLOSING}),
            new GrammarRule(GrammarRule.VAR_ARRAY_ID,
            new String[]{Token.TYPE_ARRAY_IDENTIFIER, Token.TYPE_IDENTIFIER, Token.TYPE_ARRAY_CLOSING}),
            new GrammarRule(GrammarRule.VAR_ARRAY_ID,
            new String[]{Token.TYPE_ARRAY_IDENTIFIER, Token.TYPE_ARRAY_CLOSING}),
            new GrammarRule(GrammarRule.VAR_INIT_STMT,
            new String[]{Token.TYPE_DATA_TYPE, Token.TYPE_IDENTIFIER, Token.TYPE_ASSIGNMENT, GrammarRule.VAR_STRING_CONCAT}),
            new GrammarRule(GrammarRule.VAR_INIT_STMT,
            new String[]{Token.TYPE_IDENTIFIER, Token.TYPE_ASSIGNMENT, GrammarRule.VAR_STRING_CONCAT}),
            new GrammarRule(GrammarRule.VAR_INC_STMT,
            new String[]{Token.TYPE_ADD_SUBTRACT_INC, GrammarRule.VAR_IDENTIFIER, GrammarRule.VAR_NUMERIC_CONST}),
            new GrammarRule(GrammarRule.VAR_INC_STMT,
            new String[]{Token.TYPE_ADD_SUBTRACT_DEC, GrammarRule.VAR_IDENTIFIER, GrammarRule.VAR_NUMERIC_CONST}),};

        return rules;

    }

    public String[] tokenFinalStates() {
        String[] finals = {Token.TYPE_ADD_SUBTRACT, Token.TYPE_AND_NAND,
            Token.TYPE_ARRAY_DATA_TYPE, Token.TYPE_ASSIGNMENT,
            Token.TYPE_BREAK, Token.TYPE_EXPONENTIAL,
            Token.TYPE_CLOSE_PARENTHESIS, Token.TYPE_COMMENT_SINGLE,
            Token.TYPE_DATA_TYPE, Token.TYPE_DELIMITER, Token.TYPE_DO,
            Token.TYPE_FOR, Token.TYPE_GET, Token.TYPE_CLOSE_PROGRAM_BLOCK,
            Token.TYPE_IDENTIFIER, Token.TYPE_IF, Token.TYPE_LITERAL,
            Token.TYPE_MULTIPLY_DIVIDE, Token.TYPE_NOISE,
            Token.TYPE_NUMERIC, Token.TYPE_OPEN_PARENTHESIS,
            Token.TYPE_OR_NOR, Token.TYPE_OUTPUT, Token.TYPE_PROGNAME,
            Token.TYPE_OPEN_PROGRAM_BLOCK, Token.TYPE_RELATIONAL,
            Token.TYPE_SPACE, Token.TYPE_SWITCH, Token.TYPE_THEN,
            Token.TYPE_TO_DATA_TYPE, Token.TYPE_WHILE, Token.TYPE_NOT,
            Token.TYPE_NUMERIC_FLOAT, Token.TYPE_COMMENT_MULTI,
            Token.TYPE_ELSE, Token.TYPE_COMMA, Token.TYPE_TRUE,
            Token.TYPE_FALSE, Token.TYPE_STRING_CONCAT,
            Token.TYPE_ARRAY_IDENTIFIER, Token.TYPE_ARRAY_CLOSING,
            Token.TYPE_OPEN_BRACES, Token.TYPE_CLOSE_BRACES,
            Token.TYPE_OPEN_BLOCK, Token.TYPE_CLOSE_BLOCK,
            Token.TYPE_MAYBE_BIN, Token.TYPE_MAYBE_OCT,
            Token.TYPE_HEX_CONSTANT, Token.TYPE_OCT_CONSTANT,
            Token.TYPE_BIN_CONSTANT, Token.TYPE_ADD_SUBTRACT_DEC,
            Token.TYPE_OUTPUT_2, Token.TYPE_WHEN, Token.TYPE_EOF,
            Token.TYPE_PROGRAM_ID, Token.TYPE_ETC,
            Token.TYPE_ADD_SUBTRACT_INC};

        return finals;
    }

    public String[][] tokenTable(String s) {
        String[][] table = {
            {s, "\\", "BACKLASH"},
            {"BACKLASH", "E", Token.TYPE_DELIMITER},
            {"BACKLASH", "e", Token.TYPE_DELIMITER},
            {s, "(", Token.TYPE_OPEN_PARENTHESIS},
            {Token.TYPE_OPEN_PARENTHESIS, ":", Token.TYPE_OPEN_BLOCK},
            {s, ":", "COLON"},
            {"COLON", ")", Token.TYPE_CLOSE_BLOCK},
            {s, ")", Token.TYPE_CLOSE_PARENTHESIS},
            {s, "A", "A"}, {s, "a", "A"},
            {s, "B", "B"}, {s, "b", "B"},
            {s, "C", "C"}, {s, "c", "C"},
            {s, "D", "D"}, {s, "d", "D"},
            {s, "E", "E"}, {s, "e", "E"},
            {s, "F", "F"}, {s, "f", "F"},
            {s, "G", "G"}, {s, "g", "G"},
            {s, "H", "H"}, {s, "h", "H"},
            {s, "I", "I"}, {s, "i", "I"},
            {s, "J", "DEAD"}, {s, "j", "DEAD"},
            {s, "K", "DEAD"}, {s, "k", "DEAD"},
            {s, "L", "DEAD"}, {s, "l", "DEAD"},
            {s, "M", "DEAD"}, {s, "m", "DEAD"},
            {s, "N", "N"}, {s, "n", "N"},
            {s, "O", "O"}, {s, "o", "O"},
            {s, "P", "P"}, {s, "p", "P"},
            {s, "Q", "DEAD"}, {s, "q", "DEAD"},
            {s, "R", "DEAD"}, {s, "r", "DEAD"},
            {s, "S", "S"}, {s, "s", "S"},
            {s, "T", "T"}, {s, "t", "T"},
            {s, "U", "DEAD"}, {s, "u", "DEAD"},
            {s, "V", "DEAD"}, {s, "v", "DEAD"},
            {s, "W", "W"}, {s, "w", "W"},
            {s, "X", "DEAD"}, {s, "x", "DEAD"},
            {s, "Y", "DEAD"}, {s, "y", "DEAD"},
            {s, "Z", "DEAD"}, {s, "z", "DEAD"},
            {s, "*", Token.TYPE_MULTIPLY_DIVIDE},
            {s, "/", Token.TYPE_MULTIPLY_DIVIDE},
            {s, "+", Token.TYPE_ADD_SUBTRACT},
            {s, "-", Token.TYPE_ADD_SUBTRACT},
            {"D", "E", "DE"}, {"D", "e", "DE"},
            {"DE", "C", Token.TYPE_ADD_SUBTRACT_DEC},
            {"DE", "c", Token.TYPE_ADD_SUBTRACT_DEC},
            {"I", "N", "IN"}, {"I", "n", "IN"},
            {"IN", "C", Token.TYPE_ADD_SUBTRACT_INC},
            {"IN", "c", Token.TYPE_ADD_SUBTRACT_INC},
            {s, "^", Token.TYPE_EXPONENTIAL},
            {"P", "R", "PR"}, {"P", "r", "PR"},
            {"PR", "I", "PRI"}, {"PR", "i", "PRI"},
            {"PRI", "N", "PRIN"}, {"PRI", "n", "PRIN"},
            {"PRIN", "T", Token.TYPE_OUTPUT},
            {"PRIN", "t", Token.TYPE_OUTPUT},
            {Token.TYPE_OUTPUT, "L", "PRINTL"},
            {Token.TYPE_OUTPUT, "l", "PRINTL"},
            {"PRINTL", "N", Token.TYPE_OUTPUT_2},
            {"PRINTL", "n", Token.TYPE_OUTPUT_2},
            {s, "$", "IDSTART"},
            {"IDSTART", "A-Z", Token.TYPE_IDENTIFIER},
            {"IDSTART", "a-z", Token.TYPE_IDENTIFIER},
            {"IDSTART", "0-9", Token.TYPE_IDENTIFIER},
            {Token.TYPE_IDENTIFIER, "A-Z", Token.TYPE_IDENTIFIER},
            {Token.TYPE_IDENTIFIER, "a-z", Token.TYPE_IDENTIFIER},
            {Token.TYPE_IDENTIFIER, "0-9", Token.TYPE_IDENTIFIER},
            {Token.TYPE_IDENTIFIER, "_", "IDUNDERSCORE"},
            {"IDUNDERSCORE", "A-Z", Token.TYPE_IDENTIFIER},
            {"IDUNDERSCORE", "a-z", Token.TYPE_IDENTIFIER},
            {"IDUNDERSCORE", "0-9", Token.TYPE_IDENTIFIER},
            {s, "=", Token.TYPE_ASSIGNMENT},
            {Token.TYPE_ASSIGNMENT, "=", Token.TYPE_RELATIONAL},
            {Token.TYPE_ASSIGNMENT, ">", Token.TYPE_RELATIONAL},
            {Token.TYPE_ASSIGNMENT, "<", Token.TYPE_RELATIONAL},
            {s, "<", Token.TYPE_RELATIONAL},
            {s, ">", Token.TYPE_RELATIONAL},
            {s, "~", Token.TYPE_NOT},
            {Token.TYPE_NOT, "=", Token.TYPE_RELATIONAL},
            {s, "&", Token.TYPE_AND_NAND},
            {"A", "N", "AN"}, {"A", "n", "AN"},
            {"AN", "D", Token.TYPE_AND_NAND},
            {"AN", "d", Token.TYPE_AND_NAND},
            {"N", "A", "NA"},
            {"N", "a", "NA"},
            {"NA", "N", "NAN"},
            {"NA", "n", "NAN"},
            {"NAN", "D", Token.TYPE_AND_NAND},
            {"NAN", "d", Token.TYPE_AND_NAND},
            {"S", "T", "ST"}, {"S", "t", "ST"},
            {"ST", "O", "STO"}, {"ST", "o", "STO"},
            {"STO", "P", Token.TYPE_BREAK},
            {"STO", "p", Token.TYPE_BREAK},
            {s, " ", Token.TYPE_SPACE},
            {s, "\t", Token.TYPE_SPACE},
            {s, "\n", Token.TYPE_SPACE},
            {"BACKLASH", "L", Token.TYPE_SPACE},
            {"BACKLASH", "l", Token.TYPE_SPACE},
            {"BACKLASH", "F", Token.TYPE_EOF},
            {"BACKLASH", "f", Token.TYPE_EOF},
            {"W", "H", "WH"}, {"W", "h", "WH"},
            {"WH", "E", "WHE"}, {"WH", "e", "WHE"},
            {"WHE", "N", Token.TYPE_WHEN},
            {"WHE", "n", Token.TYPE_WHEN},
            {"B", "Y", Token.TYPE_NOISE},
            {"B", "y", Token.TYPE_NOISE},
            {"N", "U", "NU"}, {"N", "u", "NU"},
            {"NU", "M", Token.TYPE_DATA_TYPE},
            {"NU", "m", Token.TYPE_DATA_TYPE},
            {"T", "E", "TE"}, {"T", "e", "TE"},
            {"TE", "X", "TEX"}, {"TE", "x", "TEX"},
            {"TEX", "T", Token.TYPE_DATA_TYPE},
            {"TEX", "t", Token.TYPE_DATA_TYPE},
            {"T", "N", "TN"}, {"T", "n", "TN"},
            {"TN", "F", Token.TYPE_DATA_TYPE},
            {"TN", "f", Token.TYPE_DATA_TYPE},
            {"B", "I", "BI"}, {"B", "i", "BI"},
            {"BI", "N", Token.TYPE_DATA_TYPE},
            {"BI", "n", Token.TYPE_DATA_TYPE},
            {"H", "E", "HE"}, {"H", "e", "HE"},
            {"HE", "X", Token.TYPE_DATA_TYPE},
            {"HE", "x", Token.TYPE_DATA_TYPE},
            {"O", "C", "OC"}, {"O", "c", "OC"},
            {"OC", "T", Token.TYPE_DATA_TYPE},
            {"OC", "t", Token.TYPE_DATA_TYPE},
            {"C", "H", "CH"}, {"C", "h", "CH"},
            {"CH", "E", "CHE"}, {"CH", "e", "CHE"},
            {"CHE", "C", "CHEC"}, {"CHE", "c", "CHEC"},
            {"CHEC", "K", Token.TYPE_SWITCH},
            {"CHEC", "k", Token.TYPE_SWITCH},
            {"PR", "O", "PRO"}, {"PR", "o", "PRO"},
            {"PRO", "G", "PROG"}, {"PRO", "g", "PROG"},
            {"PROG", "N", "PROGN"}, {"PROG", "n", "PROGN"},
            {"PROGN", "A", "PROGNA"}, {"PROGN", "a", "PROGNA"},
            {"PROGNA", "M", "PROGNAM"}, {"PROGNA", "m", "PROGNAM"},
            {"PROGNAM", "E", Token.TYPE_PROGNAME},
            {"PROGNAM", "e", Token.TYPE_PROGNAME},
            {"G", "E", "GE"}, {"G", "e", "GE"},
            {"GE", "T", Token.TYPE_GET},
            {"GE", "t", Token.TYPE_GET},
            {"T", "O", "TO"}, {"T", "o", "TO"},
            {"TO", "N", "TON"}, {"TO", "n", "TON"},
            {"TON", "U", "TONU"}, {"TON", "u", "TONU"},
            {"TONU", "M", Token.TYPE_TO_DATA_TYPE},
            {"TONU", "m", Token.TYPE_TO_DATA_TYPE},
            {"TO", "B", "TOB"}, {"TO", "b", "TOB"},
            {"TOB", "I", "TOBI"}, {"TOB", "i", "TOBI"},
            {"TOBI", "N", Token.TYPE_TO_DATA_TYPE},
            {"TOBI", "n", Token.TYPE_TO_DATA_TYPE},
            {"TO", "O", "TOO"}, {"TO", "o", "TOO"},
            {"TOO", "C", "TOOC"}, {"TOO", "c", "TOOC"},
            {"TOOC", "T", Token.TYPE_TO_DATA_TYPE},
            {"TOOC", "t", Token.TYPE_TO_DATA_TYPE},
            {"TO", "H", "TOH"}, {"TO", "h", "TOH"},
            {"TOH", "E", "TOHE"}, {"TOH", "e", "TOHE"},
            {"TOHE", "X", Token.TYPE_TO_DATA_TYPE},
            {"TOHE", "x", Token.TYPE_TO_DATA_TYPE},
            {"TO", "T", "TOT"}, {"TO", "t", "TOT"},
            {"TOT", "N", "TOTN"}, {"TOT", "n", "TOTN"},
            {"TOTN", "F", Token.TYPE_TO_DATA_TYPE},
            {"TOTN", "f", Token.TYPE_TO_DATA_TYPE},
            {"TOT", "E", "TOTE"}, {"TOT", "e", "TOTE"},
            {"TOTE", "X", "TOTEX"}, {"TOTE", "x", "TOTEX"},
            {"TOTEX", "T", Token.TYPE_TO_DATA_TYPE},
            {"TOTEX", "t", Token.TYPE_TO_DATA_TYPE},
            {s, "|", Token.TYPE_OR_NOR},
            {"O", "R", Token.TYPE_OR_NOR},
            {"O", "r", Token.TYPE_OR_NOR},
            {"N", "O", "NO"}, {"N", "o", "NO"},
            {"NO", "R", Token.TYPE_OR_NOR},
            {"NO", "r", Token.TYPE_OR_NOR},
            {"I", "F", Token.TYPE_IF},
            {"I", "f", Token.TYPE_IF},
            {"T", "H", "TH"}, {"T", "h", "TH"},
            {"TH", "E", "THE"}, {"TH", "e", "THE"},
            {"THE", "N", Token.TYPE_THEN},
            {"THE", "n", Token.TYPE_THEN},
            {"E", "L", "EL"}, {"E", "l", "EL"},
            {"EL", "S", "ELS"}, {"EL", "s", "ELS"},
            {"ELS", "E", Token.TYPE_ELSE},
            {"ELS", "e", Token.TYPE_ELSE},
            {"WH", "I", "WHI"}, {"WH", "i", "WHI"},
            {"WHI", "L", "WHIL"}, {"WHI", "l", "WHIL"},
            {"WHIL", "E", Token.TYPE_WHILE},
            {"WHIL", "e", Token.TYPE_WHILE},
            {"D", "O", Token.TYPE_DO},
            {"D", "o", Token.TYPE_DO},
            {"F", "O", "FO"},
            {"F", "o", "FO"},
            {"FO", "R", Token.TYPE_FOR},
            {"FO", "r", Token.TYPE_FOR},
            {"C", "O", "CO"}, {"C", "o", "CO"},
            {"CO", "M", "COM"}, {"CO", "m", "COM"},
            {"COM", "M", "COMM"}, {"COM", "m", "COMM"},
            {"COMM", "E", "COMME"}, {"COMM", "e", "COMME"},
            {"COMME", "N", "COMMEN"}, {"COMME", "n", "COMMEN"},
            {"COMMEN", "C", "COMMENC"}, {"COMMEN", "c", "COMMENC"},
            {"COMMENC", "E", Token.TYPE_OPEN_PROGRAM_BLOCK},
            {"COMMENC", "e", Token.TYPE_OPEN_PROGRAM_BLOCK},
            {"E", "N", "EN"}, {"E", "n", "EN"},
            {"EN", "D", Token.TYPE_CLOSE_PROGRAM_BLOCK},
            {"EN", "d", Token.TYPE_CLOSE_PROGRAM_BLOCK},
            {Token.TYPE_DATA_TYPE, "[", Token.TYPE_ARRAY_DATA_TYPE},
            {Token.TYPE_IDENTIFIER, "[", Token.TYPE_ARRAY_IDENTIFIER},
            {s, "]", Token.TYPE_ARRAY_CLOSING},
            {s, "\"", "LITANYTHING"},
            {"LITANYTHING", " ", "LITANYTHING"},
            {"LITANYTHING", "!", "LITANYTHING"},
            {"LITANYTHING", "#-[", "LITANYTHING"},
            {"LITANYTHING", "]-~", "LITANYTHING"},
            {"LITANYTHING", "\\", "LITBCKLASH"},
            {"LITBCKLASH", "\\", "LITBCKLASH"},
            {"LITBCKLASH", "\"", "LITQUOTINMID"},
            {"LITBCKLASH", "L", "DEAD"},
            {"LITBCKLASH", " ", "LITANYTHING"},
            {"LITBCKLASH", "!", "LITANYTHING"},
            {"LITBCKLASH", "#-~", "LITANYTHING"},
            {"LITQUOTINMID", "\"", Token.TYPE_LITERAL},
            {"LITQUOTINMID", "!", "LITANYTHING"},
            {"LITQUOTINMID", " ", "LITANYTHING"},
            {"LITQUOTINMID", "#-~", "LITANYTHING"},
            {"LITANYTHING", "\"", Token.TYPE_LITERAL},
            {"LITANYTHING", "\t", "LITANYTHING"},
            {"LITBCKLASH", "\t", "LITBCKLASH"},
            {"LITQUOTINMID", "\t", "LITANYTHING"}, //TODO small letters...
            {s, "8-9", Token.TYPE_NUMERIC},
            {Token.TYPE_NUMERIC, "0-9", Token.TYPE_NUMERIC},
            {Token.TYPE_NUMERIC, "A-F", "HEXMAYBEHEX"},
            {Token.TYPE_NUMERIC, "a-f", "HEXMAYBEHEX"},
            {Token.TYPE_NUMERIC, "H", Token.TYPE_HEX_CONSTANT},
            {Token.TYPE_NUMERIC, "h", Token.TYPE_HEX_CONSTANT},
            {Token.TYPE_NUMERIC, ".", "NUMMAYBEDECIMAL"},
            {"NUMMAYBEDECIMAL", "0-9", Token.TYPE_NUMERIC_FLOAT},
            {Token.TYPE_NUMERIC_FLOAT, "0-9", Token.TYPE_NUMERIC_FLOAT},
            {"BACKLASH", "\\", "CMMTSTART"},
            {"CMMTSTART", " ", "CMMTMID"},
            {"CMMTSTART", "1-~", "CMMTMID"},
            {"CMMTSTART", "\t", "CMMTMID"},
            {"CMMTSTART", "\n", "CMMTMID"},
            {"CMMTMID", " ", "CMMTMID"},
            {"CMMTMID", "!-[", "CMMTMID"},
            {"CMMTMID", "]-~", "CMMTMID"},
            {"CMMTMID", "\t", "CMMTMID"},
            {"CMMTMID", "\n", "CMMTMID"},
            {"CMMTEO", " ", "CMMTMID"},
            {"CMMTEO", "!-K", "CMMTMID"},
            {"CMMTEO", "M-~", "CMMTMID"},
            {"CMMTEO", "\t", "CMMTMID"},
            {"CMMTEO", "\n", "CMMTMID"},
            {"CMMTMID", "\\", "CMMTEO"},
            {"CMMTEO", "L", Token.TYPE_COMMENT_SINGLE}, //TODO small letters...
            {"BACKLASH", "C", "MCMMTMID"},
            {"BACKLASH", "c", "MCMMTMID"},
            {"MCMMTMID", " ", "MCMMTMID"},
            {"MCMMTMID", "!-B", "MCMMTMID"},
            {"MCMMTMID", "D-b", "MCMMTMID"},
            {"MCMMTMID", "d-~", "MCMMTMID"},
            {"MCMMTMID", "\t", "MCMMTMID"},
            {"MCMMTMID", "\n", "MCMMTMID"},
            {"MCMMTMID", "C", "MCMMTC"},
            {"MCMMTMID", "c", "MCMMTC"},
            {"MCMMTC", " ", "MCMMTMID"},
            {"MCMMTC", "!-[", "MCMMTMID"},
            {"MCMMTC", "]-~", "MCMMTMID"},
            {"MCMMTC", "\t", "MCMMTMID"},
            {"MCMMTC", "\n", "MCMMTMID"}, //TODO small letters...
            {"MCMMTC", "\\", Token.TYPE_COMMENT_MULTI},
            {s, ",", Token.TYPE_COMMA},
            {"T", "R", "TR"}, {"T", "r", "TR"},
            {"TR", "U", "TRU"}, {"TR", "u", "TRU"},
            {"TRU", "E", Token.TYPE_TRUE},
            {"TRU", "e", Token.TYPE_TRUE},
            {"F", "A", "FA"}, {"F", "a", "FA"},
            {"FA", "L", "FAL"}, {"FA", "l", "FAL"},
            {"FAL", "S", "FALS"}, {"FAL", "s", "FALS"},
            {"FALS", "E", Token.TYPE_FALSE},
            {"FALS", "e", Token.TYPE_FALSE},
            {s, "{", Token.TYPE_OPEN_BRACES},
            {s, "}", Token.TYPE_CLOSE_BRACES},
            {s, ".", Token.TYPE_STRING_CONCAT},
            {s, "0-1", Token.TYPE_MAYBE_BIN},
            {Token.TYPE_MAYBE_BIN, "0-1", Token.TYPE_MAYBE_BIN},
            {Token.TYPE_MAYBE_BIN, "B", Token.TYPE_BIN_CONSTANT},
            {Token.TYPE_MAYBE_BIN, "b", Token.TYPE_BIN_CONSTANT},
            {Token.TYPE_MAYBE_BIN, "O", Token.TYPE_OCT_CONSTANT},
            {Token.TYPE_MAYBE_BIN, "o", Token.TYPE_OCT_CONSTANT},
            {Token.TYPE_MAYBE_BIN, "H", Token.TYPE_HEX_CONSTANT},
            {Token.TYPE_MAYBE_BIN, "h", Token.TYPE_HEX_CONSTANT},
            {Token.TYPE_MAYBE_BIN, "2-7", Token.TYPE_MAYBE_OCT},
            {Token.TYPE_MAYBE_BIN, "8-9", Token.TYPE_NUMERIC},
            {Token.TYPE_BIN_CONSTANT, "H", Token.TYPE_HEX_CONSTANT},
            {Token.TYPE_BIN_CONSTANT, "h", Token.TYPE_HEX_CONSTANT},
            {Token.TYPE_MAYBE_BIN, "A", "HEXMAYBEHEX"},
            {Token.TYPE_MAYBE_BIN, "a", "HEXMAYBEHEX"},
            {Token.TYPE_MAYBE_BIN, "C-F", "HEXMAYBEHEX"},
            {Token.TYPE_MAYBE_BIN, "c-f", "HEXMAYBEHEX"},
            {Token.TYPE_BIN_CONSTANT, "A-F", "HEXMAYBEHEX"},
            {Token.TYPE_BIN_CONSTANT, "a-f", "HEXMAYBEHEX"},
            {Token.TYPE_BIN_CONSTANT, "0-9", "HEXMAYBEHEX"},
            {Token.TYPE_MAYBE_BIN, ".", "NUMMAYBEDECIMAL"},
            {s, "2-7", Token.TYPE_MAYBE_OCT},
            {Token.TYPE_MAYBE_OCT, "0-7", Token.TYPE_MAYBE_OCT},
            {Token.TYPE_MAYBE_OCT, "O", Token.TYPE_OCT_CONSTANT},
            {Token.TYPE_MAYBE_OCT, "o", Token.TYPE_OCT_CONSTANT},
            {Token.TYPE_MAYBE_OCT, "H", Token.TYPE_HEX_CONSTANT},
            {Token.TYPE_MAYBE_OCT, "h", Token.TYPE_HEX_CONSTANT},
            {Token.TYPE_MAYBE_OCT, "8-9", Token.TYPE_NUMERIC},
            {Token.TYPE_MAYBE_OCT, "A-F", "HEXMAYBEHEX"},
            {Token.TYPE_MAYBE_OCT, "a-f", "HEXMAYBEHEX"},
            {Token.TYPE_MAYBE_OCT, ".", "NUMMAYBEDECIMAL"},
            {"HEXMAYBEHEX", "A-F", "HEXMAYBEHEX"},
            {"HEXMAYBEHEX", "a-f", "HEXMAYBEHEX"},
            {"HEXMAYBEHEX", "0-9", "HEXMAYBEHEX"},
            {"HEXMAYBEHEX", "H", Token.TYPE_HEX_CONSTANT},
            {"HEXMAYBEHEX", "h", Token.TYPE_HEX_CONSTANT},
            {"A", "H", Token.TYPE_HEX_CONSTANT},
            {"A", "h", Token.TYPE_HEX_CONSTANT},
            {"A", "A-F", "HEXMAYBEHEX"},
            {"A", "a-f", "HEXMAYBEHEX"},
            {"A", "0-9", "HEXMAYBEHEX"},
            {"B", "H", Token.TYPE_HEX_CONSTANT},
            {"B", "h", Token.TYPE_HEX_CONSTANT},
            {"B", "A-F", "HEXMAYBEHEX"},
            {"B", "a-f", "HEXMAYBEHEX"},
            {"B", "0-9", "HEXMAYBEHEX"},
            {"C", "H", Token.TYPE_HEX_CONSTANT},
            {"C", "h", Token.TYPE_HEX_CONSTANT},
            {"C", "A-F", "HEXMAYBEHEX"},
            {"C", "a-f", "HEXMAYBEHEX"},
            {"C", "0-9", "HEXMAYBEHEX"},
            {"D", "H", Token.TYPE_HEX_CONSTANT},
            {"D", "h", Token.TYPE_HEX_CONSTANT},
            {"D", "A-D", "HEXMAYBEHEX"},
            {"D", "a-d", "HEXMAYBEHEX"},
            {"DE", "A-D", "HEXMAYBEHEX"},
            {"DE", "a-d", "HEXMAYBEHEX"},
            {Token.TYPE_ADD_SUBTRACT_DEC, "A-D", "HEXMAYBEHEX"},
            {Token.TYPE_ADD_SUBTRACT_DEC, "a-d", "HEXMAYBEHEX"},
            {Token.TYPE_ADD_SUBTRACT_DEC, "H", Token.TYPE_HEX_CONSTANT},
            {Token.TYPE_ADD_SUBTRACT_DEC, "h", Token.TYPE_HEX_CONSTANT},
            {"DE", "H", Token.TYPE_HEX_CONSTANT},
            {"DE", "h", Token.TYPE_HEX_CONSTANT},
            {"D", "F", "HEXMAYBEHEX"},
            {"D", "f", "HEXMAYBEHEX"},
            {"D", "0-9", "HEXMAYBEHEX"},
            {"E", "H", Token.TYPE_HEX_CONSTANT},
            {"E", "h", Token.TYPE_HEX_CONSTANT},
            {"E", "A-F", "HEXMAYBEHEX"},
            {"E", "a-f", "HEXMAYBEHEX"},
            {"E", "0-9", "HEXMAYBEHEX"},
            {"F", "H", Token.TYPE_HEX_CONSTANT},
            {"F", "h", Token.TYPE_HEX_CONSTANT},
            {"F", "A-F", "HEXMAYBEHEX"},
            {"F", "a-f", "HEXMAYBEHEX"},
            {"F", "0-9", "HEXMAYBEHEX"},
            {s, "#", "PROGIDSTART"},
            {"PROGIDSTART", "A-Z", Token.TYPE_PROGRAM_ID},
            {"PROGIDSTART", "a-z", Token.TYPE_PROGRAM_ID},
            {"PROGIDSTART", "0-9", Token.TYPE_PROGRAM_ID},
            {Token.TYPE_PROGRAM_ID, "A-Z", Token.TYPE_PROGRAM_ID},
            {Token.TYPE_PROGRAM_ID, "a-z", Token.TYPE_PROGRAM_ID},
            {Token.TYPE_PROGRAM_ID, "0-9", Token.TYPE_PROGRAM_ID},
            {Token.TYPE_PROGRAM_ID, "_", "PROGIDUNDERSCORE"},
            {"PROGIDUNDERSCORE", "A-Z", Token.TYPE_PROGRAM_ID},
            {"PROGIDUNDERSCORE", "a-z", Token.TYPE_PROGRAM_ID},
            {"PROGIDUNDERSCORE", "0-9", Token.TYPE_PROGRAM_ID},
            {"E", "T", "ET"}, {"E", "t", "ET"},
            {"ET", "C", Token.TYPE_ETC},
            {"ET", "c", Token.TYPE_ETC},
            {s, "#-%", "DEAD"},
            {s, "'", "DEAD"},
            {s, "`", "DEAD"},
            {s, ";", "DEAD"},
            {s, "?", "DEAD"},
            {s, "@", "DEAD"},
            {s, "[", "DEAD"},
            {s, "_", "DEAD"},
            {s, "!", "DEAD"}
        };
        return table;
    }

    public void p(String m) {
        System.out.println(m);
    }

    public void p(int m) {
        System.out.println(m);
    }

    public void p(char m) {
        System.out.println(m);
    }

    public void p(boolean m) {
        System.out.println(m);
    }

    public void p(String[][] m) {

        for (int i = 0; i < m.length; i++) {
            System.out.print(i + "\t");
            for (int j = 0; j < m[i].length; j++) {
                System.out.print(m[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
