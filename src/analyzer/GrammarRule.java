package analyzer;

import scanner.Token;

public class GrammarRule extends Token{
	
	private String leftHandSide;

	private String[] rightHandSide;
        
        public static final String VAR_START = "[S]";
        public static final String VAR_BLOCK = "[block]";
        public static final String VAR_STATEMENT = "[stmt]";
        public static final String VAR_IF_STMT = "[ifthen]";
        public static final String VAR_ELSE_STMT = "[else]";
        public static final String VAR_WHILE_STMT = "[while]";
        public static final String VAR_DO_WHILE_STMT = "[dowhile]";
        public static final String VAR_FOR_STMT = "[for]";
        public static final String VAR_SWITCH_STMT = "[check]";
        public static final String VAR_PRINT_STMT = "[print]";
        public static final String VAR_GET_STMT = "[get]";
        public static final String VAR_INC_STMT = "[inc]";
        public static final String VAR_DECLARE_STMT = "[dec]";
        public static final String VAR_DECLARE_STMT_PUMPAH = "[dec_p]";
        public static final String VAR_INIT_STMT = "[init]";
        public static final String VAR_ARRAY_DECLARE_STMT = "[arr_dec]";
        public static final String VAR_ARRAY_INIT_STMT = "[arr_init]";
        public static final String VAR_ARRAY_CONTENTS = "[arr_cont]";
        public static final String VAR_ARRAY_CONTENTS_PUMPAH = "[arr_cont_p]";
        public static final String VAR_SWITCH_BLOCK = "[check_blck]";
        public static final String VAR_IDENTIFIER = "[id]";
        public static final String VAR_ARRAY_ID = "[arr_id]";
        public static final String VAR_CONSTANT = "[const]";
        public static final String VAR_NUMERIC_CONST = "[num_const]";
        public static final String VAR_MATH_EXP = "[math_exp]";
        public static final String VAR_MATH_TERM = "[math_term]";
        public static final String VAR_MATH_FACTOR = "[math_factor]";
        public static final String VAR_MATH_POWER = "[math_power]";
        public static final String VAR_MATH_CAST = "[math_cast]";
        public static final String VAR_BOOLEAN_EXP = "[bool_exp]";
        public static final String VAR_BOOLEAN_OR = "[bool_or]";
        public static final String VAR_BOOLEAN_AND = "[bool_and]";
        public static final String VAR_BOOLEAN_LGE = "[bool_lge]";
        public static final String VAR_BOOLEAN_NOT = "[bool_not]";
        public static final String VAR_STRING_CONCAT = "[str_concat]";
	
	public GrammarRule(String leftHandSide, String[] rightHandSide){
		super(leftHandSide, leftHandSide);
	
		this.leftHandSide = leftHandSide;
		this.rightHandSide = rightHandSide;
	}
	
	
	public String leftHandSide(){
		return leftHandSide;
	}
	
	public void setLeftHandSide(String leftHandSide){
		this.leftHandSide = leftHandSide;
		
	}
	
	public String[] rightHandSide(){
		return rightHandSide;
	}
	
	public void setRightHandSide(String[] rightHandSide){
		this.rightHandSide = rightHandSide;
		
	}
	
	@Override
	public String toString() {
		return leftHandSide.toString();
	}
	
	public String rule(){

		String derivation = "";
		
		for(int i = 0; i < rightHandSide.length; i++){
			if(i == rightHandSide.length-1)
				derivation += (rightHandSide[i]);
			else derivation += (rightHandSide[i] + " ");
			
		}
		
		return leftHandSide.toString()
				+ " -> " + derivation; 
	}
}
