package jankiel.utils;

import java.util.ArrayList;
import java.util.Stack;

public class FixInfix {
	
    public static int filled;
	
    public static void main(String[] args) {
	
	}
	
    public  static char[] toCharArray(String inp){
        char[] c = new char[255];
        for (filled = 0; filled < inp.length(); filled++) {
            c[filled] = inp.charAt(filled);
        }
        return c;
    }
    public static double evalPrefix(String zzz){
        String[] Y = zzz.split((" "));
        ArrayList<String> toX = new ArrayList<String>();
        for(int i = 0; i < Y.length; i++){
        	if(Y[i].equals(" ") || Y[i].equals("")) continue;
        	else toX.add(Y[i]);
        }
        String[] X = new String[toX.size()];
        for(int i = 0; i < toX.size(); i++){
        	X[i] = toX.get(i);
        }
        for(int i = 0; i < X.length; i++){
        	System.out.println(X[i]);
        }
        Stack<Double> s = new Stack<Double>();
        double a=0, b=0;
        for (int i = X.length-1; i >= 0; i--) {
            if(!isOperator(X[i])) s.push(Double.parseDouble(X[i]));
            else{
                a = s.pop();
                b = s.pop();
                s.push(eval(a, X[i], b));
            }
        }
        return s.pop();
    }
	
    public static double eval(double x, String o, double y){
        double w = 0;
        switch(o.charAt(0)){
            case '+': w = x+y; break;
            case '-': w = x-y; break;
            case '*': w = x*y; break;
            case '/': w = x/y; break;
            case '^': w = Math.pow(x, y); break;
            default: break;
        }
        return w;
    }
	
    public static String toPrefix(String zzz){
        char[] X = toCharArray(zzz);
        StringBuffer sk = new StringBuffer("");
        Stack<Character> rev = new Stack<Character>();
        Stack<Character> optr = new Stack<Character>();
        for (int i = filled-1; i >= 0 ; i--) {
            if(!isOperator(X[i])) rev.push(X[i]);
            else if (X[i] == '('){
                rev.push(' ');
                while(optr.peek() != ')'){
                    rev.push(optr.pop());
                rev.push(' ');
                }
                if(optr.peek() == ')') optr.pop();
            }
            else{
                rev.push(' ');
                while(!optr.isEmpty() && !isEqualPos(optr.peek(), X[i]) &&
                        getIncPrio(X[i]) < getInPrio(optr.peek()))
                    if(X[i] == ')') break;
                    else{
                        rev.push(optr.pop()); 
                rev.push(' ');
                    }
                optr.push(X[i]);
            }
        }
        while(!optr.isEmpty()){
                rev.push(' ');
            rev.push(optr.pop());
        }
        while(!rev.isEmpty()) sk.append(rev.pop());
        return sk.toString();
    }
	
    public static boolean isEqualPos(char a, char b){
        return (getIncPrio(a) == getIncPrio(b) &&
                getInPrio(a) == getInPrio(b));
    }
	
    public static boolean isOperator(char c){
        switch(c){
            case '^': return true;
            case '*': return true;
            case '/': return true;
            case '+': return true;
            case '-': return true;
            case '(': return true;
            case ')': return true;
            default: return false;
        }
    }
	
    public static boolean isOperator(String c){
        switch(c.charAt(0)){
            case '^': return true;
            case '*': return true;
            case '/': return true;
            case '+': return true;
            case '-': return true;
            case '(': return true;
            case ')': return true;
            default: return false;
        }
    }
	
    public static int getIncPrio(char c){
        switch(c){
            case '^': return 6;
            case '*': return 3;
            case '/': return 3;
            case '+': return 1;
            case '-': return 1;
            case '(': return 9;
            default: return -1;
        }
    }
	
    public static int getInPrio(char c){
        switch(c){
            case '^': return 5;
            case '*': return 4;
            case '/': return 4;
            case '+': return 2;
            case '-': return 2;
            case '(': return 0;
            default: return -1;
        }
    }
}
