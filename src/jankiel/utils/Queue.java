package jankiel.utils;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Chona Arboleda
 */
public class Queue<T> {
    SLL<T> sl = new SLL<T>();
    public Queue(){

    }
    public void clear(){
        sl.makeNull();
    }
    public boolean isEmpty(){
        return sl.isEmpty();
    }
    public boolean isFull(){
        return false;
    }
    public void enqueue(T el){
        sl.addToTail(el);
    }
    public T dequeue(){
        return sl.deleteFromHead();
    }
    public T firstElt(){
        return sl.getHeadInfo();
    }
}
