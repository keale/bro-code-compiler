package jankiel.utils;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import analyzer.SLLNode;

/**
 *
 * @author jankiel
 */
public class Tree<T> {

    private TreeNode<T> root;

    public Tree() {
        root = null;
    }

    public Tree(TreeNode<T> node) {
        root = node;
    }

    public int maxHeight() {
        return 0;
    }

    public SLL<TreeNode<T>> leftMostPath() {
        return leftMostPath(root);
    }

    public SLL<TreeNode<T>> leftMostPath(TreeNode<T> node) {

        SLL<TreeNode<T>> paths = new SLL<TreeNode<T>>();

        if (node.children() == null) {
            SLL<TreeNode<T>> sll = new SLL<TreeNode<T>>();
            sll.addToTail(node);
            return sll;
        }
        paths.addToHead(node);
        paths.concat(leftMostPath(node.children().getHead().info()));

        return paths;
    }

    public SLL<TreeNode<T>> shortestPath() {
        return shortestPath(root);
    }

    public SLL<TreeNode<T>> shortestPath(TreeNode<T> node) {

        if (node.children() == null) {
            SLL<TreeNode<T>> sll = new SLL<TreeNode<T>>();
            sll.addToTail(node);
            return sll;
        }

        SLL<TreeNode<T>> children = node.children();
        SLL<SLL<TreeNode<T>>> depths = new SLL<SLL<TreeNode<T>>>();

        SLLNode<TreeNode<T>> ptr = children.getHead();

        while (ptr != null) {
            depths.addToTail(shortestPath(ptr.info()));
            ptr = ptr.next();
        }

        SLLNode<SLL<TreeNode<T>>> p = depths.getHead();
        SLLNode<SLL<TreeNode<T>>> minSLL;

        int min;
        if (p != null) {
            min = p.info().length();
            minSLL = p;
            p = p.next();
        } else {
            return null;
        }

        while (p != null) {
            if (p.info().length() <= min) {
                min = p.info().length();
                minSLL = p;
            }
            p = p.next();
        }

        minSLL.info().addToHead(node);

        return minSLL.info();
    }

    public SLL<TreeNode<T>> longestPath() {
        return longestPath(root);
    }

    public SLL<TreeNode<T>> longestPath(TreeNode<T> node) {

        if (node.children() == null) {
            SLL<TreeNode<T>> sll = new SLL<TreeNode<T>>();
            sll.addToTail(node);
            return sll;
        }

        SLL<TreeNode<T>> children = node.children();
        SLL<SLL<TreeNode<T>>> depths = new SLL<SLL<TreeNode<T>>>();

        SLLNode<TreeNode<T>> ptr = children.getHead();

        while (ptr != null) {
            depths.addToTail(longestPath(ptr.info()));
            ptr = ptr.next();
        }

        SLLNode<SLL<TreeNode<T>>> p = depths.getHead();
        SLLNode<SLL<TreeNode<T>>> maxSLL;

        int max;
        if (p != null) {
            max = p.info().length();
            maxSLL = p;
            p = p.next();
        } else {
            return null;
        }

        while (p != null) {
            if (p.info().length() <= max) {
                max = p.info().length();
                maxSLL = p;
            }
            p = p.next();
        }

        maxSLL.info().addToHead(node);

        return maxSLL.info();
    }

    public TreeNode<T> root() {
        return root;
    }

    public void setRoot(TreeNode<T> root) {
        this.root = root;
    }

    public int minDepth() {
        return minDepth(root);
    }

    public int minDepth(TreeNode<T> node) {

        if (node.children() == null) {
            return 1;
        }

        SLL<TreeNode<T>> children = node.children();
        SLL<Integer> depths = new SLL<Integer>();

        SLLNode<TreeNode<T>> ptr = children.getHead();

        while (ptr != null) {
            depths.addToTail(minDepth(ptr.info()) + 1);
            ptr = ptr.next();
        }

        SLLNode<Integer> p = depths.getHead();

        int min;
        if (p != null) {
            min = p.info();
            p = p.next();
        } else {
            return 0;
        }

        while (p != null) {
            if (p.info() <= min) {
                min = p.info();
            }
            p = p.next();
        }

        return min;
    }

    public int maxDepth() {
        return maxDepth(root);
    }

    public int maxDepth(TreeNode<T> node) {

        if (node.children() == null) {
            return 1;
        }

        SLL<TreeNode<T>> children = node.children();
        SLL<Integer> depths = new SLL<Integer>();

        SLLNode<TreeNode<T>> ptr = children.getHead();

        while (ptr != null) {
            depths.addToTail(maxDepth(ptr.info()) + 1);
            ptr = ptr.next();
        }

        SLLNode<Integer> p = depths.getHead();

        int max;
        if (p != null) {
            max = p.info();
            p = p.next();
        } else {
            return 0;
        }

        while (p != null) {
            if (p.info() > max) {
                max = p.info();
            }
            p = p.next();
        }

        return max;
    }

    public String printBreadthFirst(String s) {

        StringBuffer sk = new StringBuffer("");
        TreeNode<T> p = root;
        Queue<TreeNode<T>> q = new Queue<TreeNode<T>>();
        TreeNode<T> ln = new TreeNode<T>();

        if (p != null) {
            q.enqueue(p);

            while (!q.isEmpty()) {
                p = q.dequeue();
                if (p != ln) {
                    sk.append(p.visit(s));
                } else {
                    sk.append("");
                }
                //child 
                SLL<TreeNode<T>> children = p.children();

                if (children != null) {
                    q.enqueue(ln);
                    SLLNode<TreeNode<T>> ptr = children.getHead();

                    while (ptr != null) {
                        q.enqueue(ptr.info());
                        ptr = ptr.next();
                    }
                }
            }
        }

        return sk.toString();
    }

    public String printGrouped() {
        return "RT=" + printGrouped(root);
    }

    public String printGrouped(TreeNode<T> n) {
        StringBuffer sk = new StringBuffer("");
        if (n != null) {
            sk.append("(").append(n.visit(" ")).append("C=");
            SLL<TreeNode<T>> children = n.children();

            if (children != null) {
                SLLNode<TreeNode<T>> ptr = children.getHead();

                while (ptr != null) {
                    sk.append(printGrouped(ptr.info()));
                    ptr = ptr.next();
                }
            }

            sk.append(")");
        } else {
            sk.append("\\");
        }
        return sk.toString();
    }

    @Override
    public String toString() {
        return printGrouped();
    }

    public String printPreorderR(String s) {
        return printPreorderR(root, s);
    }

    public String printPreorderR(TreeNode<T> p, String s) {
        StringBuffer sk = new StringBuffer("");
        sk.append(p.visit(s));

        SLL<TreeNode<T>> children = p.children();

        if (children != null) {
            SLLNode<TreeNode<T>> ptr = children.getHead();

            while (ptr != null) {
                sk.append(printPreorderR(ptr.info(), s));
                ptr = ptr.next();
            }
        }
        return sk.toString();
    }

    public static void test(String[] args) {

        Tree<String> t = new Tree<String>();

        TreeNode<String> A = new TreeNode("A");
        TreeNode<String> B = new TreeNode("B");
        TreeNode<String> C = new TreeNode("C");
        TreeNode<String> D = new TreeNode("D");
        TreeNode<String> E = new TreeNode("E");
        TreeNode<String> F = new TreeNode("F");
        TreeNode<String> G = new TreeNode("G");
        TreeNode<String> H = new TreeNode("H");
        TreeNode<String> I = new TreeNode("I");
        TreeNode<String> J = new TreeNode("J");

        SLL<TreeNode<String>> HIJ = new SLL<TreeNode<String>>();
        HIJ.addToTail(H);
        HIJ.addToTail(I);
        HIJ.addToTail(J);

        SLL<TreeNode<String>> EF = new SLL<TreeNode<String>>();
        EF.addToTail(E);
        EF.addToTail(F);

        SLL<TreeNode<String>> BCD = new SLL<TreeNode<String>>();
        BCD.addToTail(B);
        BCD.addToTail(C);
        BCD.addToTail(D);

        F.setChildren(HIJ);

        D.setChildren(G);

        C.setChildren(EF);

        A.setChildren(BCD);

        t.setRoot(A);

        System.out.println(t.printPreorderR(" "));
        System.out.println(t.printGrouped());
    }

    public static void test2(String[] args) {

        Tree<String> t = new Tree<String>();

        TreeNode<String> A = new TreeNode("4");
        TreeNode<String> C = new TreeNode("4");
        TreeNode<String> E = new TreeNode("+");

        SLL<TreeNode<String>> plus = new SLL<TreeNode<String>>();
        plus.addToTail(A);
        plus.addToTail(C);

        E.setChildren(plus);

        t.setRoot(E);

        System.out.println(t.printPreorderR(" "));
        System.out.println(t.printGrouped());
    }

    public void toExpressionTree() {
        
//        if(root.children() != null && root.children().length() == 1)

        while (root.children() != null && root.children().length() == 1) {
            setRoot(root.children().getHeadInfo());
        }

        if (root.element().toString().charAt(0) == '[') {
            setRoot(toExpressionTree(root));
        }
    }

    public TreeNode<T> toExpressionTree(TreeNode<T> node) {
        SLL<TreeNode<T>> children = node.children();

        if (children != null) {
            if (children.length() == 3 && children.getHead().next.info().element().toString().charAt(0) != '[') {
                SLL<TreeNode<T>> newChildren = new SLL<TreeNode<T>>();
                newChildren.addToTail(toExpressionTree(children.getHeadInfo()));
                newChildren.addToTail(toExpressionTree(children.getTailInfo()));

                TreeNode<T> midNode = children.getHead().next.info();
                midNode.setChildren(newChildren);
                if(node.parent().toString().equals("[print]"))
                    node.setChildren(midNode);
                else node.parent().setChildren(midNode);

                return midNode;
            }

            if (children.length() == 2) {
                SLL<TreeNode<T>> newChildren = new SLL<TreeNode<T>>();
                newChildren.addToTail(toExpressionTree(children.getTailInfo()));

                TreeNode<T> midNode = children.getHeadInfo();
                midNode.setChildren(newChildren);

                node.parent().setChildren(midNode);
                return midNode;
            }

            if (children.length() == 1) {

                TreeNode<T> n = toExpressionTree(children.getHeadInfo());

                node.parent().setChildren(n);

                return n;
            }

            if (children.getHead().next.info().element().toString().charAt(0) == '[') {

                TreeNode<T> n = toExpressionTree(children.getHead().next.info());

                node.parent().setChildren(n);

                return n;

            }

        }

        return node;
    }
}