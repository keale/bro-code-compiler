package jankiel.utils;

import analyzer.SLLNode;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * This SLL class was made by the "God of CS - UST"
 * CJD, and other methods were added by pahlavi5312
 * @author 3csd-14
 */
public class SLL<T>{
    private SLLNode<T> head, tail;
    public SLL() { // constructor
        head = tail = null;
    }
    public int length(){
        if(head == null) return 0;
        if(head == tail) return 1;
        SLLNode<T> ptr = head;
        int l = 0;
        while(ptr != null){
            l++;
            ptr = ptr.next;
        }
        return l;
    }
    public boolean isEmpty() {
        return head == null;
    }
    public void addToHead(T el) {
    head = new SLLNode<T>(el,head);
    if (tail == null)
        tail = head; // only node of list
    }
    public void addToTail(T el) {
    if (isEmpty())
        head = tail = new SLLNode<T>(el);
    else {
        tail.next = new SLLNode<T>(el);
        tail = tail.next;
        }
    }
    public T deleteFromHead() {
        if (isEmpty())
            return null; //nothing to delete
        T el = head.info;
        if (head == tail) // only one node
            head = tail = null; // now empty list
        else
            head = head.next; // next node is head
        return el;
    }
    public T deleteFromTail() {
        if (isEmpty()) return null;
            T el = tail.info;
        if (head == tail) // only one node
            head = tail = null;
        else {
            SLLNode<T> p; // find predecessor of tail
            for (p = head; p.next != tail; p = p.next) ;
            tail = p; // predecessor of tail is the new tail
            tail.next = null;
        }
        return el;
    }
    public String toString() { // you can use this for toString()
        SLLNode<T> p; String s = "";
        for (p = head; p != null; p = p.next)
            s=s+p.info.toString() + " ";
        return s;
    }
    public String printAll() { // you can use this for toString()
        SLLNode<T> p; String s = "";
        for (p = head; p != null; p = p.next)
            s=s+p.info.toString() + " ";
        return s;
    }
    public SLLNode<T> getNode(T el) {
        SLLNode<T> p = head;
        while (p != null && p.info != el) p = p.next;
            return p; // a node if found, null if not
    }
    public SLLNode<T> getHead(){
        return head;
    }
    public T getHeadInfo(){
        return head.info;
    }
    public SLLNode<T> getTail(){
        return tail;
    }
    public T getTailInfo(){
        return tail.info;
    }
    public SLLNode<T> getNextNode(SLLNode<T> p){
        return p.next;
    }
    public boolean isInList(T el) {
        return getNode(el) != null; // true if found, false if not
    }
    public void insertAfterNode(T el, SLLNode<T> ptr ) {
        if (ptr != null)
            if (tail == ptr) addToTail(el);
            else ptr.next = new SLLNode<T>(el, ptr.next);
    }
    public void insertAfterElt(T elOld, T elNew) {
        SLLNode<T> p;
        p = getNode(elOld); // find node with el
        if (p != null) insertAfterNode(elNew, p); // found, insert after
    }
    public void insertBefElt(T elOld, T elNew){
        if(getNode(elOld) == head) addToHead(elNew);
        else{
            SLLNode<T> pred, t, i; // find el¡¯s predecessor
            for (pred = head, t = head.next;
                t != null && t.info != elOld;
                pred = pred.next, t = t.next);
            //t = new SLLNode<T>(elNew, t);
            for (i = tail; i != t; i = getPredNode(i.info) ) {
                i = new SLLNode<T>(i.info, i.next);
            }
            i.next = new SLLNode<T>(i.info, i.next);
            t.info = elNew;
        }
    }
    public SLLNode<T> getPredNode(T el){
        SLLNode<T> pred, t; // find el¡¯s predecessor
        for (pred = head, t = head.next;
            t != null && t.info != el;
            pred = pred.next, t = t.next);
        return pred;
    }
    public SLLNode<T> getPredNode(SLLNode<T> x){
        SLLNode<T> pred, t; // find el’s predecessor
        for (pred = head, t = head.next;
            t != null && t != x;
            pred = pred.next, t = t.next);
        return pred;
    }
    public T delete(T el) { // delete node with element el
        if (isEmpty()) return null;
        if (el == head.info) return deleteFromHead();
        if (el == tail.info) return deleteFromTail();
        SLLNode<T> pred, t; // find el¡¯s predecessor
        for (pred = head, t = head.next;
            t != null && t.info != el;
            pred = pred.next, t = t.next) ;
        if (t == null) return null; // if el was not found
        else {
            pred.next = t.next;
            return el; }
    }
    public T delete(SLLNode<T> p){
        SLLNode<T> pred, t; T el = p.info;
        if(p == null) System.out.println("Error");
        else {
            if(p == head) return deleteFromHead();
            else{
                for (pred = head, t = head.next;
                    t != null && t != p;
                    pred = pred.next, t = t.next);
                if(t.next == null){
                    pred.next = null;
                    tail = pred;
                }
                else pred.next = t.next;
            }
        }
        return el;
    }
    public T deleteNode(SLLNode<T> p){
        T el = p.info;
        getPredNode(p).next = p.next;
        return el;
    }
    public T deleteDups(T el) { // delete node with element el and its duplicates
       if(isEmpty()) return null;
       else{
           SLLNode<T> i;
           for(i = head; i != null; i = i.next){
               if(i.info == el) deleteNode(i);
           }
       }
       return el;
    }
    public T deleteDups(T el, int n) { // delete node with element el and its n duplicates
       if(isEmpty()) return null;
       else{
           SLLNode<T> i;
           for(i = head; i != null && n > 0; i = i.next){
               if(i.info == el){
                   deleteNode(i);
                   n--;
               }
           }
       }
       return el;
    }
    public void makeNull(){
        head = tail = null;
    }
    public void swap(SLLNode<T> lef, SLLNode<T> ryt){
        SLLNode<T> temp;
        temp = lef; lef = ryt; ryt = lef;
    }
    public boolean similarCheck(SLL a, SLL b){
        SLLNode<T> i, j;
        if(a.isEmpty() && b.isEmpty()) return true;
        else if(!a.isEmpty() && b.isEmpty()) return false;
        else if(a.isEmpty() && !b.isEmpty()) return false;
        for (i = a.head, j = b.head; i != null && j != null;
            i = i.next, j = j.next) {
            if(!i.info.equals(j.info)) return false;
        }
        return true;
    }
    public boolean equals(SLL a){
        SLLNode<T> i, j;
        if(a.isEmpty() && this.isEmpty()) return true;
        else if(!a.isEmpty() && this.isEmpty()) return false;
        else if(a.isEmpty() && !this.isEmpty()) return false;
        for (i = a.head, j = this.head; i != null && j != null;
            i = i.next, j = j.next) {
            if(!i.info.equals(j.info)) return false;
        }
        return true;
    }
    public int countNodes(){
        SLLNode<T> j = head; int i;
        for (i = 0; j != null; j = j.next, i++);
        return i;
    }
    public SLL<T> copyInsertIndex(){
        SLL<T> c = new SLL<T>();
        SLLNode<T> i = head; Integer j;
        for (j = 0; i != null; j++, i = i.next) {
            c.addToTail((T) j);
        }
        return c;
    }
    public void concat(SLL<T> other){
        tail.next = other.head;
        tail = other.tail;
    }
    /*public SLL<T> mergeSort(SLL<T> l1,SLL<T> l2){
    SLL<T> out = new SLL<T>();
    while(l1.head != null && l2.head != null){
        if (l1.head.info < l2.head.info){
            out.addToTail(l1.head.info);
            l1.head = l1.head.next;
        }
        else {
            out.addToTail(l2.head.info);
            l2.head = l2.head.next;
        }
    }
    if (l1.head != null)    {
      for (SLLNode<T> i = l1.head; i != null; i = i.next)      {
          out.addToTail(l1.head.info);
      }
    }
    else    {
      for (SLLNode<T> i = l2.head; i != null; i = i.next)      {
          out.addToTail(l2.head.info);
      }
    }
    indexC = 0;
    for (i = finalStart; i <= finalEnd; i++)    {
      list[i] = listC[indexC];
      indexC = indexC + 1;
    }
    return out;
  }
     public void insertSorted(T el){
        if(isEmpty()) addToHead(el);
        else{
            if(el <= head.info) addToHead(el);
            else{
                SLLNode<T> pred, t;
                for (pred = head.next, t = head.next.next;
                t != null && el > t.info;
                pred = pred.next, t = t.next);
                insertAfterNode(el, pred);
            }
        }
    }
    public void insertSorted(T el, boolean isDesc){
        if(!isDesc) insertSorted(el);
        else{
             if(isEmpty()) head.info = tail.info = el;
                else{
                    if(el >= head.info) addToHead(el);
                    else{
                        SLLNode<T> pred, t;
                        for (pred = head.next, t = head.next.next;
                        t != null && el < t.info;
                        pred = pred.next, t = t.next);
                        insertAfterNode(el, pred);
                    }
                }
        }
    }*/
}

