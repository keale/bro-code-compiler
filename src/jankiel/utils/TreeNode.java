/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jankiel.utils;

import analyzer.SLLNode;

/**
 *
 * @author Gian
 */
public class TreeNode<T>{
	private TreeNode<T> parent;
	private T el;
	private SLL<TreeNode<T>> children;

	public TreeNode(){
		parent = null;
		el = null;
		children = null;
	}

	public TreeNode(T element){
		parent = null;
		el = element;
		children = null;
	}

	public TreeNode(T element, SLL<TreeNode<T>> childs){
		parent = null;
		el = element;
		children = childs;
	}

	public TreeNode(TreeNode<T> par, T element, SLL<TreeNode<T>> childs){
		parent = par;
		el = element;
		children = childs;
	}

	public void setParent(TreeNode<T> par){
		parent = par;
	}

	public void setElement(T element){
		el = element;
	}

	public void setChildren(SLL<TreeNode<T>> childs){
		children = childs;

		SLLNode<TreeNode<T>> ptr = children.getHead();
		while(ptr != null){

			ptr.info.setParent(this);
			ptr = ptr.next;
		}

	}

	public void addToChildren(TreeNode<T> child){
		children.addToTail(child);
	}

	public void setChildren(TreeNode<T> child){
		SLL<TreeNode<T>> c = new SLL<TreeNode<T>>();
		c.addToTail(child);
		children = c;

		child.setParent(this);
	}
        
        //first kuya
        public TreeNode<T> firstSiblingChild(){
            return parent.children.getHeadInfo();
        }
        
        //panganay
        public TreeNode<T> firstChild(){
            return children.getHeadInfo();
        }

	public TreeNode<T> parent(){
		return parent;
	}

	public T element(){
		return el;
	}

	public SLL<TreeNode<T>> children(){
		return children;
	}

	public String visit(String s) {
		return el.toString() + s;
	}

	@Override
	public String toString() {
		return el.toString() + " ";
	}
}

