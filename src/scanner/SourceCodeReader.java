package scanner;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * @author Gian
 */
public class SourceCodeReader {

	private Scanner read;

	private String currentLine;

	public int possibleStartOfTokenPtr;
	public int possibleEndOfTokenPtr;
	
	private int lineNumber;
	private int positionOnTheLine;
	
	private final int START_OF_LINE = 0;
	private final int JUST_AFTER_THE_START = 1;
	private final int INSIDE_THE_LINE = 2;

	public SourceCodeReader(String filename) throws FileNotFoundException {
		read = new Scanner(new FileReader(filename));
		currentLine = "";

		lineNumber = 0;

		possibleStartOfTokenPtr = possibleEndOfTokenPtr = 0;
	}

	public char getNextChar() {
		if (possibleEndOfTokenPtr >= currentLine.length()) {
			if (read.hasNextLine()) {
				currentLine += (read.nextLine() + "\\L");
				positionOnTheLine =  START_OF_LINE;
				
			}
		}
		if(positionOnTheLine == START_OF_LINE) positionOnTheLine = JUST_AFTER_THE_START;
		else if(positionOnTheLine == JUST_AFTER_THE_START){
			lineNumber++;
			positionOnTheLine = INSIDE_THE_LINE;
		}
		return currentLine.charAt(possibleEndOfTokenPtr++);
	}

	public void returnChar() {
		if (possibleEndOfTokenPtr > 0) {
			possibleEndOfTokenPtr--;
		}
	}

	public void advanceChar() {
		if (possibleEndOfTokenPtr < currentLine.length()) {
			possibleEndOfTokenPtr++;
		}
	}

	public String getLexeme() {

		String lexeme = currentLine.substring(possibleStartOfTokenPtr,
				possibleEndOfTokenPtr);

		possibleStartOfTokenPtr = possibleEndOfTokenPtr;

		return lexeme;
	}

	public boolean hasNextChar() {
		return (currentLine.length() > possibleEndOfTokenPtr)
				|| read.hasNextLine();
	}

	public int lineNumber() {
		return lineNumber;
	}
}
