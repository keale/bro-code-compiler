package scanner;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * @author user
 */
public class Token {
	private String tokenType;
	private String lexeme;
	private String idType;
	private String idValue;
	private String printValue;
	private String scope;
	private String range;
	private String location;

	public static final String TYPE_DELIMITER = "sts";
	public static final String TYPE_OPEN_BLOCK = "o_bls";
	public static final String TYPE_CLOSE_BLOCK = "c_bls";
	public static final String TYPE_MULTIPLY_DIVIDE = "ao3";
	public static final String TYPE_ADD_SUBTRACT = "ao4";
	public static final String TYPE_ADD_SUBTRACT_INC = "ao4_i";
	public static final String TYPE_ADD_SUBTRACT_DEC = "ao4_d";
	public static final String TYPE_EXPONENTIAL = "ao2";
	public static final String TYPE_OUTPUT = "pro";
	public static final String TYPE_OUTPUT_2 = "pro_2";
	public static final String TYPE_COMMENT_SINGLE = "coi_s";
	public static final String TYPE_COMMENT_MULTI = "coi_m";
	public static final String TYPE_IDENTIFIER = "idt";
	public static final String TYPE_ARRAY_IDENTIFIER = "a_idt";
	public static final String TYPE_RELATIONAL = "reo";
	public static final String TYPE_AND_NAND = "lo1";
	public static final String TYPE_BREAK = "bro";
	public static final String TYPE_SPACE = "spt";
	public static final String TYPE_NOISE = "not";
	public static final String TYPE_OPEN_PARENTHESIS = "o_pat";
	public static final String TYPE_CLOSE_PARENTHESIS = "c_pat";
	public static final String TYPE_ASSIGNMENT = "aso";
	public static final String TYPE_DATA_TYPE = "dtt";
	public static final String TYPE_SWITCH = "swo";
	public static final String TYPE_PROGNAME = "pnt";
	public static final String TYPE_GET = "ino";
	public static final String TYPE_TO_DATA_TYPE = "pao";
	public static final String TYPE_OR_NOR = "lo2";
	public static final String TYPE_IF = "ifs";
	public static final String TYPE_THEN = "ths";
	public static final String TYPE_ELSE = "els";
	public static final String TYPE_WHILE = "whs";
	public static final String TYPE_DO = "dos";
	public static final String TYPE_FOR = "fos";
	public static final String TYPE_ARRAY_DATA_TYPE = "adt";
	public static final String TYPE_LITERAL = "lit";
	public static final String TYPE_NUMERIC = "nct";
	public static final String TYPE_NUMERIC_FLOAT = "nct_f";
	public static final String TYPE_NOT = "lo3";
	public static final String TYPE_COMMA = "cma";
	public static final String TYPE_TRUE = "tru";
	public static final String TYPE_FALSE = "fls";
	public static final String TYPE_ERROR = "err";
	public static final String TYPE_OPEN_BRACES = "o_brc";
	public static final String TYPE_CLOSE_BRACES = "c_brc";
	public static final String TYPE_STRING_CONCAT = "co1";
	public static final String TYPE_ARRAY_CLOSING = "a_cls";
	public static final String TYPE_OPEN_PROGRAM_BLOCK = "o_pbt";
	public static final String TYPE_CLOSE_PROGRAM_BLOCK = "c_pbt";
	public static final String TYPE_MAYBE_BIN = "mb_nct";
	public static final String TYPE_MAYBE_OCT = "mo_nct";
	public static final String TYPE_HEX_CONSTANT = "hct";
	public static final String TYPE_OCT_CONSTANT = "oct";
	public static final String TYPE_BIN_CONSTANT = "bct";
	public static final String TYPE_PROGRAM_ID = "p_idt";
	public static final String TYPE_WHEN = "who";
	public static final String TYPE_EOF = "eof";
	public static final String TYPE_ETC = "eto";
        
	public static final String DATA_TYPE_NUM = "num";
	public static final String DATA_TYPE_TEXT = "text";
	public static final String DATA_TYPE_TNF = "tnf";
	public static final String DATA_TYPE_HEX = "hex";	
	public static final String DATA_TYPE_OCT = "oct";
	public static final String DATA_TYPE_BIN = "bin";
        
	public static final String SYNTAX_NEXT_LINE = "nl";

	public Token(String tokenType, String lexeme) {
		this.tokenType = tokenType;
		this.lexeme = lexeme;
                
                idType = "";
                idValue = "";
                printValue = "";
                scope = "";
                range = "";
                location = "";
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public String type() {
		return tokenType;
	}

	public void setLexeme(String lexeme) {
		this.lexeme = lexeme;
	}

	public String lexeme() {
		return lexeme;
	}

	@Override
	public String toString() {
//		return tokenType + ": " + lexeme;
//		return tokenType;
		return lexeme;
//		if (tokenType.equals(TYPE_IDENTIFIER)
//				|| tokenType.equals(TYPE_ARRAY_IDENTIFIER)
//				|| tokenType.equals(TYPE_LITERAL)
//				|| tokenType.equals(TYPE_NUMERIC)
//				|| tokenType.equals(TYPE_MAYBE_OCT)
//				|| tokenType.equals(TYPE_MAYBE_BIN)
//				|| tokenType.equals(TYPE_NUMERIC_FLOAT)
//				|| tokenType.equals(TYPE_HEX_CONSTANT)
//				|| tokenType.equals(TYPE_OCT_CONSTANT)
//				|| tokenType.equals(TYPE_BIN_CONSTANT))
//			return lexeme;
//		else if (tokenType.equals(TYPE_ERROR))
//			return "Error: " + lexeme;
//		else
//			return tokenType;
	}
	
	public boolean equals(Token token){
		return this.tokenType.equals(token.type()) 
			&& this.lexeme.equals(token.lexeme());
	}
	
	public static String sampleLexeme(String type){
		if(type.equals(TYPE_DELIMITER)) return "\\e";
		if(type.equals(TYPE_OPEN_BLOCK)) return "{";
		if(type.equals(TYPE_CLOSE_BLOCK)) return "}";
		if(type.equals(TYPE_MULTIPLY_DIVIDE)) return "multiply operation";
		if(type.equals(TYPE_ADD_SUBTRACT)) return "add operation";
		if(type.equals(TYPE_ADD_SUBTRACT_INC)) return "inc";
		if(type.equals(TYPE_ADD_SUBTRACT_DEC)) return "dec";
		if(type.equals(TYPE_EXPONENTIAL)) return "^";
		if(type.equals(TYPE_OUTPUT)) return "prDouble";
		if(type.equals(TYPE_OUTPUT_2)) return "prDoubleln";
		if(type.equals(TYPE_IDENTIFIER)) return "an identifier";
		if(type.equals(TYPE_ARRAY_IDENTIFIER)) return "an array identifier";
		if(type.equals(TYPE_RELATIONAL)) return "a relational operator";
		if(type.equals(TYPE_AND_NAND)) return "&";
		if(type.equals(TYPE_BREAK)) return "stop";
		if(type.equals(TYPE_OPEN_PARENTHESIS)) return "(";
		if(type.equals(TYPE_CLOSE_PARENTHESIS)) return ")";
		if(type.equals(TYPE_ASSIGNMENT)) return "=";
		if(type.equals(TYPE_DATA_TYPE)) return "a data type";
		if(type.equals(TYPE_SWITCH)) return "check";
		if(type.equals(TYPE_PROGNAME)) return "progname";
		if(type.equals(TYPE_GET)) return "get";
		if(type.equals(TYPE_TO_DATA_TYPE)) return "a cast operation";
		if(type.equals(TYPE_OR_NOR)) return "|";
		if(type.equals(TYPE_IF)) return "if";
		if(type.equals(TYPE_THEN)) return "then";
		if(type.equals(TYPE_ELSE)) return "else";
		if(type.equals(TYPE_WHILE)) return "while";
		if(type.equals(TYPE_DO)) return "do";
		if(type.equals(TYPE_FOR)) return "for";
		if(type.equals(TYPE_ARRAY_DATA_TYPE)) return "an array data type";
		if(type.equals(TYPE_LITERAL)) return "a literal";
		if(type.equals(TYPE_NUMERIC)) return "a number";
		if(type.equals(TYPE_MAYBE_OCT)) return "a number";
		if(type.equals(TYPE_MAYBE_BIN)) return "a number";
		if(type.equals(TYPE_NUMERIC_FLOAT)) return "a floating poDouble number";
		if(type.equals(TYPE_NOT)) return "~";
		if(type.equals(TYPE_COMMA)) return ",";
		if(type.equals(TYPE_TRUE)) return "true";
		if(type.equals(TYPE_FALSE)) return "false";
		if(type.equals(TYPE_OPEN_BRACES)) return "{";
		if(type.equals(TYPE_CLOSE_BRACES)) return "}";
		if(type.equals(TYPE_STRING_CONCAT)) return ".";
		if(type.equals(TYPE_ARRAY_CLOSING)) return "]";
		if(type.equals(TYPE_OPEN_PROGRAM_BLOCK)) return "commence";
		if(type.equals(TYPE_CLOSE_PROGRAM_BLOCK)) return "end";
		if(type.equals(TYPE_HEX_CONSTANT)) return "a hexadecimal number";
		if(type.equals(TYPE_OCT_CONSTANT)) return "an octal number";
		if(type.equals(TYPE_BIN_CONSTANT)) return "a binary number";
		if(type.equals(TYPE_PROGRAM_ID)) return "a program id";
		if(type.equals(TYPE_WHEN)) return "when";
		if(type.equals(TYPE_ETC)) return "etc";
		return type;
	}
        
        public String idType(){
            return idType;
        }
        
        public void setIdType(String dataType){
            idType = dataType;
        }
        
        public String idValue(){
            return idValue;
        }
        
        public void setIdValue(String value){
            idValue = value;
        }
        
        public String printValue(){
            return printValue;
        }
        
        public void setPrintValue(String value){
            printValue = value;
        }
        
        public String properties(){
            return "Type: " + tokenType 
                    + "\nLexeme: " + lexeme
                    + "\nData Type: " + idType
                    + "\nValue: " + idValue
                    + "\nWhen Printed: " + printValue;
        }
}
