package scanner;

import java.util.ArrayList;


public class DFA {

	private String[][] transTable;
	private String startState;
	private String[] finalStates;
	private String state;

	private final int CURRENT_STATE = 0;
	private final int INPUT_CHARACTER = 1;
	private final int NEXT_STATE = 2;

	private ArrayList<String> states;

	public DFA(String start, String[][] table, String[] stateFinal) {

		states = new ArrayList<String>();
		
		transTable = table;
		finalStates = stateFinal;
		startState = start;

		states.add(startState);

		for(int i = 0; i < table.length; i++){
			
			if(!table[i][0].equals("") && !states.contains(table[i][0]))
				states.add(table[i][0]);
			if(!table[i][2].equals("") && !states.contains(table[i][2]))
				states.add(table[i][2]);
		}

		for(int i = 0; i < finalStates.length && finalStates != null; i++){
			if(!finalStates[i].equals("") &&  !states.contains(finalStates[i]))
				states.add(finalStates[i]);
		}
	}

	public int move(char inputChar) {
		for (int i = 0; i < transTable.length; i++) {
			if (transTable[i][CURRENT_STATE].equals(state)) {
				if (transTable[i][INPUT_CHARACTER].length() > 1) {
					if (isInSet(transTable[i][INPUT_CHARACTER],
							Character.toString(inputChar))) {
						state = transTable[i][NEXT_STATE];
						return 0;
					}
				} else if (transTable[i][INPUT_CHARACTER].equals(Character
						.toString(inputChar))) {
					state = transTable[i][NEXT_STATE];
					return 0;
				}
			}
		}
		state = null;
		return 0;
	}

	public int move(String input) {
		for (int i = 0; i < transTable.length; i++) {
			if (transTable[i][CURRENT_STATE].equals(state)) {
				if (transTable[i][INPUT_CHARACTER].equals(input)) {
					state = transTable[i][NEXT_STATE];
					return 0;
				}
			}
		}
		state = null;
		return 0;
	}

	public boolean accepts(String input) {

		char[] inputArr = input.toCharArray();
		state = startState;

		for (int i = 0; i < inputArr.length && state != null; i++) {
			move(inputArr[i]);
		}

		return (state != null) && isInFinalStates(state);
	}

	public boolean isInFinalStates(String state) {

		for (int i = 0; i < finalStates.length; i++)
			if (state.equals(finalStates[i]))
				return true;

		return false;
	}

	public boolean isInSet(String set, String input) {
		char startOfSet = set.charAt(0);
		char endOfSet = set.charAt(2);
		for (; startOfSet <= endOfSet; startOfSet++) {
			if (input.charAt(0) == startOfSet)
				return true;
		}
		return false;
	}

	public String state() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String startState() {
		return startState;
	}

	public void restart() {
		state = startState;
	}

	public static void test(String[] args) {

		String start = "START";
		String[] finals = {"FINAL"};
		String[][] blockTable = {
			{start, "1", "FINAL"},
			{"FINAL", "0-1", "FINAL"},
			{start, "0", null}};

		DFA dfa = new DFA(start, blockTable, finals);

	}

	public int stateCount(){
		return states.size();
	}
}
