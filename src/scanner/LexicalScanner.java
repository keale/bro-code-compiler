package scanner;

import java.io.FileNotFoundException;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Gian
 */
public class LexicalScanner {

    public SourceCodeReader reader;
    private DFA dfa;
    
    private String lastAcceptedState;
    
    private int moves;
    private int positionOfLastAccepted;
    
    private boolean isEOF;

    public LexicalScanner(String fileName, DFA dfa)
            throws FileNotFoundException {
        reader = new SourceCodeReader(fileName);
        this.dfa = dfa;
        isEOF = false;
    }

    public Token getToken() {

        lastAcceptedState = "";
        char readChar = ' ';
        dfa.restart();
        moves = positionOfLastAccepted = 0;

        if (reader.hasNextChar()) {
            while (reader.hasNextChar() && dfa.state() != null) {

                readChar = reader.getNextChar();

                dfa.move(readChar);

                moves++;

                if (dfa.state() != null) {
                    if (dfa.isInFinalStates(dfa.state())) {
                        lastAcceptedState = dfa.state();
                        positionOfLastAccepted = moves;
                    }
                }
            }

            if (reader.hasNextChar()) {
                reader.returnChar();
                dfa.setState(lastAcceptedState);
            } else {
                isEOF = true;
            }

            //Unknown Character is read
            if (dfa.state().equals(dfa.startState()) || dfa.state().equals("")
                    || !dfa.isInFinalStates(dfa.state())) {

                reader.returnChar();

                char c = reader.getNextChar();

                dfa.setState(Token.TYPE_ERROR);
                reader.getLexeme();

                return new Token(Token.TYPE_ERROR, "Unknown character " + c
                        + " read at line " + reader.lineNumber());
            } else if (moves > positionOfLastAccepted + 1) {
                while (moves > positionOfLastAccepted + 1) {
                    reader.returnChar();
                    moves--;
                }
            }

            return new Token(dfa.state(), reader.getLexeme());
        } else {
            isEOF = false;
            return new Token(Token.TYPE_EOF, "\\F");
        }
    }

    public boolean hasNext() {
        return reader.hasNextChar() || isEOF;
    }

    public static void test(String[] args) throws FileNotFoundException {
        String[][] table = {{"S", " -~", "F"}, {"F", " -~", "F"}};
        String[] F = {"F"};

        DFA df = new DFA("S", table, F);

        System.out.println();
    }

    public int lineNumber() {
        return reader.lineNumber();
    }
}
