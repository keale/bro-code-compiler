
import analyzer.SyntaxAnalyzer;
import interpreter.SemanticInterpreter;
import jankiel.utils.Tree;
import scanner.Token;

import java.io.FileNotFoundException;

import javax.swing.JOptionPane;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author user
 */
public class Tester {

    private static SyntaxAnalyzer analyzer;
    
    private static SemanticInterpreter interpreter;
    private static Tree<Token> parseTree;
    
    private static boolean verboseMode;

    public static void main(String[] args) throws FileNotFoundException {
        
        verboseMode = false;
        
        String input = JOptionPane.showInputDialog("Enter the filename");

        analyzer = new SyntaxAnalyzer(input, verboseMode);
        parseTree = analyzer.analyzeSyntax();
        if(verboseMode)
            System.out.println(parseTree.toString());

        interpreter = new SemanticInterpreter(parseTree);
        interpreter.interpret();

    }
}
