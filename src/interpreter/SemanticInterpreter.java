/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interpreter;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

import analyzer.GrammarRule;
import analyzer.SLLNode;
import analyzer.SymbolTable;
import jankiel.utils.SLL;
import jankiel.utils.Tree;
import jankiel.utils.TreeNode;
import java.text.DecimalFormat;
import scanner.Token;

/**
 *
 * @author user
 */
public class SemanticInterpreter {
    
    private Tree<Token> parseTree;
    
    private TreeNode<Token> currentNode;
    private SLL<TreeNode<Token>> currentChildren;

    public static final String TODO_INIT = "t_init";
    public static final String TODO_DEC = "t_dec";
    public static final String TODO_PRINT = "t_print";
    public static final String TODO_GET = "t_get";
    public static final String TODO_IF = "t_if";
    public static final String TODO_ELSE = "t_else";
    public static final String TODO_CHECK = "t_check";
    public static final String TODO_WHILE = "t_while";
    public static final String TODO_DOWHILE = "t_dowhile";
    public static final String TODO_FOR = "t_for";
    public static final String TODO_BREAK = "t_break";
    public static final String TODO_INC = "t_inc";
    public static final String TODO_ADEC = "t_adec";
    public static final String TODO_AINIT = "t_ainit";
    public static final String TODO_CHECKBLOCK = "t_checkblck";
    
    private String currentId;
    
    private String tempDataType;
    
    private String printType;

    private boolean ifCondition;
    private boolean flag;
    
    private boolean checkCondition;
    private TreeNode<Token> checkSibling;
    private boolean flag2;
    private boolean flag3;
    
    private boolean whileCondition;
    private TreeNode<Token> whileNode;
    
    private boolean dowhileCondition;
    private TreeNode<Token> dowhileNode;
    
    private String incType;
    
    private TreeNode<Token> tempNode;
    
    private boolean forCondition;
    private TreeNode<Token> forNode;
    private TreeNode<Token> forBoolNode;
    private String todoOnFor;
    private int bctr;
    
    public SemanticInterpreter(Tree<Token> parseTree){
        
        this.parseTree = parseTree;
        currentNode = parseTree.root();
        
        tempDataType = "";
        currentId = "";
        
        printType = "";

        ifCondition = true;
        flag = false;
        
        checkCondition = true;
        flag2 = false;
        flag3 = false;
        
        whileCondition = false;
        
        dowhileCondition = true;
        
        incType = "";
        
        forCondition = false;
        todoOnFor = "init";
        bctr = -1;
    }
    
    public void interpret() {
        traverse("");
    }
    

    public void doSomething() {
        Token element = currentNode.element();

        if (element.toString().equals(GrammarRule.VAR_INIT_STMT)) {
            traverse(TODO_INIT);
        }
        if (element.toString().equals(GrammarRule.VAR_DECLARE_STMT)) {
            traverse(TODO_DEC);
        }
        if (element.toString().equals(GrammarRule.VAR_PRINT_STMT)) {
            traverse(TODO_PRINT);
        }
        if (element.toString().equals(GrammarRule.VAR_GET_STMT)) {
            traverse(TODO_GET);
        }
        if (element.toString().equals(GrammarRule.VAR_IF_STMT)) {
            traverse(TODO_IF);
        }
        if (element.toString().equals(GrammarRule.VAR_SWITCH_STMT)) {
            traverse(TODO_CHECK);
        }
        if (element.toString().equals(GrammarRule.VAR_WHILE_STMT)) {
            whileNode = currentNode;
            traverse(TODO_WHILE);
        }
        if (element.toString().equals(GrammarRule.VAR_DO_WHILE_STMT)) {
            dowhileNode = currentNode;
            traverse(TODO_DOWHILE);
        }
        if (element.toString().equals(GrammarRule.VAR_FOR_STMT)) {
            forNode = currentNode;
            traverse(TODO_FOR);
        }
        if (element.type().equals(GrammarRule.TYPE_BREAK)) {
            traverse(TODO_BREAK);
        }
        if (element.toString().equals(GrammarRule.VAR_INC_STMT)) {
            traverse(TODO_INC);
        }
        if (element.toString().equals(GrammarRule.VAR_ARRAY_DECLARE_STMT)) {
            traverse(TODO_ADEC);
        }
        if (element.toString().equals(GrammarRule.VAR_ARRAY_INIT_STMT)) {
            traverse(TODO_AINIT);
        }
        if (element.toString().equals(GrammarRule.VAR_ARRAY_INIT_STMT)) {
            traverse(TODO_AINIT);
        }

    }

    public void initStatement() {
        Token element = currentNode.element();

        if (element.type().equals(Token.TYPE_DATA_TYPE)) {
            tempDataType = element.lexeme();
        }

        if (element.type().equals(Token.TYPE_IDENTIFIER)) {
            Token id = SymbolTable.table.get(element.lexeme());
            
            id.setIdType(tempDataType);
            currentId = id.lexeme();

        }
        
        if (element.type().equals(GrammarRule.VAR_STRING_CONCAT)) {
            Tree<Token> tree = new Tree<Token>(currentNode);

            tree.toExpressionTree();
            String prefixExp = tree.printPreorderR(" S@ ");

            Token id = SymbolTable.table.get(currentId);

            if (!id.idType().equals("")) {
                DecimalFormat df = new DecimalFormat("0");
                String evaluated = evalPrefix(prefixExp);
                
                if (typeOf(evaluated).equals(Token.DATA_TYPE_NUM)
                        && (id.idType().equals(Token.DATA_TYPE_NUM)
                        || id.idType().equals(Token.DATA_TYPE_BIN)
                        || id.idType().equals(Token.DATA_TYPE_OCT)
                        || id.idType().equals(Token.DATA_TYPE_HEX))) {
                    
                    id.setIdValue(evaluated);
                    
                    if (id.idType().equals(Token.DATA_TYPE_HEX)) {
                        String converted = convert(16, df.format(Double.parseDouble(evaluated)), 10);

                        id.setPrintValue(converted + "H");
                    } else if (id.idType().equals(Token.DATA_TYPE_OCT)) {
                        String converted = convert(8, df.format(Double.parseDouble(evaluated)), 10);

                        id.setPrintValue(converted + "O");
                    } else if (id.idType().equals(Token.DATA_TYPE_BIN)) {
                        String converted = convert(2, df.format(Double.parseDouble(evaluated)), 10);

                        id.setPrintValue(converted + "B");
                    } else if (id.idType().equals(Token.DATA_TYPE_NUM)) {
                        id.setPrintValue(formatDouble(evaluated));
                    }
                } else if (typeOf(evaluated).equals(Token.DATA_TYPE_TNF)
                        && id.idType().equals(Token.DATA_TYPE_TNF)) {
                    
                    id.setIdValue(evaluated);
                    id.setPrintValue(evaluated);
                    
                } else if (typeOf(evaluated).equals(Token.DATA_TYPE_TEXT)
                        && id.idType().equals(Token.DATA_TYPE_TEXT)) {
                    
                    id.setIdValue(evaluated);
                    id.setPrintValue(evaluated);
                    
                } else {
                    System.out.println("Error! " + typeOf(evaluated) + " detected. Data type must be " + id.idType() + ".");
                    
                }
            } else {
                System.out.println("Error! " + id.lexeme() + " not declared.");
                
            }
        }
    }

    public void decStatement() {
        Token element = currentNode.element();

        if (element.type().equals(Token.TYPE_DATA_TYPE)) {
            tempDataType = element.lexeme();
        }

        if (element.type().equals(Token.TYPE_IDENTIFIER)) {
            Token id = SymbolTable.table.get(element.lexeme());

            id.setIdType(tempDataType);
        }
    }

    public void getStatement() {
        Token element = currentNode.element();

        if (element.type().equals(Token.TYPE_IDENTIFIER)) {
            Scanner scan = new Scanner(System.in);

            Token id = SymbolTable.table.get(element.lexeme());

            String evaluated = scan.nextLine();
            
            if (!id.idType().equals("")) {
                if (id.idType().equals(Token.DATA_TYPE_TEXT)) {
                    id.setIdValue("\"" + evaluated + "\"");
                    id.setPrintValue("\"" + evaluated + "\"");
                    
                } else if (typeOf(evaluated).equals(id.idType())) {
                    id.setIdValue(evaluated);
                    id.setPrintValue(evaluated);

                } else {
                    System.out.println("Error! " + typeOf(evaluated)
                            + " detected. Data type must be " + id.idType()
                            + ".");
                }
            } else {
                System.out.println("Error! " + id.lexeme() + " not declared.");
            }
        }
    }

    public void printStatement() {
        Token element = currentNode.element();
        
        if (element.type().equals(Token.TYPE_OUTPUT)
                || element.type().equals(Token.TYPE_OUTPUT_2)) {
            printType = element.lexeme();
            
        } else if (element.type().equals(GrammarRule.VAR_STRING_CONCAT)
                || element.type().equals(Token.TYPE_STRING_CONCAT)) {
            Tree<Token> tree = new Tree<Token>(currentNode);
            
            tree.toExpressionTree();
            String prefixExp = tree.printPreorderR(" S@ ");
            String evaluated = evalPrefix(prefixExp);
            
            String toPrint = "";
            if(typeOf(evaluated).equals(Token.DATA_TYPE_NUM))
                toPrint = formatDouble(evaluated);
            else if(evaluated.charAt(0) == '$'){
                Token id = SymbolTable.table.get(evaluated);
                toPrint = id.printValue();
            }
            else toPrint = evaluated;
            
            if (!toPrint.equals("")) {
                if (toPrint.charAt(toPrint.length() - 1) == '"') {
                    toPrint = toPrint.substring(0, toPrint.length() - 1);
                }
                if (toPrint.charAt(0) == '"') {
                    toPrint = toPrint.substring(1);
                }
            }
            
            if (printType.equals("print")) {
                System.out.print(toPrint);
            } else {
                System.out.println(toPrint);
            }
        }
    }

    public void ifStatement() {
        Token element = currentNode.element();

        if (element.type().equals(GrammarRule.VAR_BOOLEAN_EXP)) {
            Tree<Token> tree = new Tree<Token>(currentNode);

            tree.toExpressionTree();
            String prefixExp = tree.printPreorderR(" S@ ");

            ifCondition = Boolean.parseBoolean(evalPrefix(prefixExp));
        }

        if(element.type().equals(GrammarRule.VAR_BLOCK) && !flag){
            if(ifCondition){
                traverse("");
                flag = true;
            }
        }

        if(element.type().equals(GrammarRule.VAR_ELSE_STMT)){
            if(!ifCondition)
                traverse(TODO_ELSE);
        }
    }

    public void elseStatement() {
        Token element = currentNode.element();
        if(element.type().equals(GrammarRule.VAR_BLOCK)){
            traverse("");
        }
    }

    public void checkStatement() {
        Token element = currentNode.element();
        
        if(!flag2){
            checkSibling = getNextSibling(currentNode);
            flag2 = true;
        }
        
        if(element.type().equals(Token.TYPE_IDENTIFIER)){
            Token id = SymbolTable.table.get(element.lexeme());
            
            currentId = id.lexeme();
        }
        
        if(element.type().equals(GrammarRule.VAR_SWITCH_BLOCK)){
            traverse(TODO_CHECKBLOCK);
        }

    }
    
    public void checkblockStatement(){
        Token element = currentNode.element();
        if(element.type().equals(Token.TYPE_TRUE)
                || element.type().equals(Token.TYPE_FALSE)
                || element.type().equals(Token.TYPE_LITERAL)
                || element.type().equals(Token.TYPE_NUMERIC)
                || element.type().equals(Token.TYPE_NUMERIC_FLOAT)
                || element.type().equals(Token.TYPE_MAYBE_BIN)
                || element.type().equals(Token.TYPE_MAYBE_OCT)
                || element.type().equals(Token.TYPE_HEX_CONSTANT)
                || element.type().equals(Token.TYPE_OCT_CONSTANT)
                || element.type().equals(Token.TYPE_BIN_CONSTANT)){
            Token id = SymbolTable.table.get(currentId);
            
            checkCondition = Boolean.parseBoolean(compute(id.idValue(), "==", element.idValue()));
        }
        
        if(element.type().equals(GrammarRule.VAR_BLOCK)){
            if(checkCondition){
                traverse("");
                flag3 = true;
            
                currentNode = checkSibling;
            }
        }
        
        if(element.type().equals(Token.TYPE_ETC)){
            if(!flag3){
                currentNode = getNextSibling(currentNode);
                traverse("");
            }
        }
    }
    
    public void whileStatement(){
        Token element = currentNode.element();
        
        if (element.type().equals(GrammarRule.VAR_BOOLEAN_EXP)) {
            Tree<Token> tree = new Tree<Token>(currentNode);

            tree.toExpressionTree();
            String prefixExp = tree.printPreorderR(" S@ ");

            whileCondition = Boolean.parseBoolean(evalPrefix(prefixExp));
        }
        
        if(element.type().equals(GrammarRule.VAR_BLOCK)){
            if(whileCondition){
                traverse("");
                currentNode = whileNode;
            }
        }
    }
    
    public void dowhileStatement(){
        Token element = currentNode.element();
        
        if (element.type().equals(GrammarRule.VAR_BOOLEAN_EXP)) {
            Tree<Token> tree = new Tree<Token>(currentNode);

            tree.toExpressionTree();
            String prefixExp = tree.printPreorderR(" S@ ");

            dowhileCondition = Boolean.parseBoolean(evalPrefix(prefixExp));
            
            if(dowhileCondition){
                currentNode = dowhileNode;
            }
        }
        
        if(element.type().equals(GrammarRule.VAR_BLOCK)){
            if(dowhileCondition){
                traverse("");
            }
        }
    }
    
    //TODO
    public void breakStatement(){
        
        tempNode = currentNode.firstSiblingChild();
        
        while(!tempNode.element().type().equals(Token.TYPE_FOR)
                && !tempNode.element().type().equals(Token.TYPE_DO)
                && !tempNode.element().type().equals(Token.TYPE_WHILE)){
            tempNode = tempNode.parent().firstSiblingChild();
        }
        
        tempNode = getNextSibling(tempNode.parent());
        
        currentNode = tempNode;
    }
    
    public void incStatement(){
        Token element = currentNode.element();
        
        if (element.type().equals(Token.TYPE_ADD_SUBTRACT_INC)
                || element.type().equals(Token.TYPE_ADD_SUBTRACT_DEC)) {
            incType = element.lexeme();
        }
        
        if (element.type().equals(Token.TYPE_IDENTIFIER)) {
            Token id = SymbolTable.table.get(element.lexeme());
            
            currentId = id.lexeme();
        }
        
        if(element.type().equals(Token.TYPE_NUMERIC)
                || element.type().equals(Token.TYPE_NUMERIC_FLOAT)
                || element.type().equals(Token.TYPE_MAYBE_BIN)
                || element.type().equals(Token.TYPE_MAYBE_OCT)){
            Token id = SymbolTable.table.get(currentId);
            
            if(!id.idType().equals("")){
                String toInc = element.lexeme();
                
                if(incType.equals("inc")){
                    String computed = compute(id.idValue(), "+", toInc);
                    id.setIdValue(computed);
                    id.setPrintValue(computed);
                    
                } else if(incType.equals("dec")){
                    String computed = compute(id.idValue(), "+", toInc);
                    id.setIdValue(computed);
                    id.setPrintValue(computed);
                    
                }
                
            } else {
                System.out.println("Error! " + id.lexeme() + " not declared.");
            }
        }
        
        if(element.type().equals(Token.TYPE_HEX_CONSTANT)){
            Token id = SymbolTable.table.get(currentId);
            
            if(!id.idType().equals("")){
                String toInc = convert(10, element.lexeme().substring(0, element.lexeme().length()-2), 16);
                
                if(incType.equals("inc")){
                    String computed = compute(id.idValue(), "+", toInc);
                    id.setIdValue(computed);
                    id.setPrintValue(computed);
                    
                } else if(incType.equals("dec")){
                    String computed = compute(id.idValue(), "+", toInc);
                    id.setIdValue(computed);
                    id.setPrintValue(computed);
                    
                }
                
            } else {
                System.out.println("Error! " + id.lexeme() + " not declared.");
            }
        }
        
        if(element.type().equals(Token.TYPE_OCT_CONSTANT)){
            Token id = SymbolTable.table.get(currentId);
            
            if(!id.idType().equals("")){
                String toInc = convert(10, element.lexeme().substring(0, element.lexeme().length()-2), 8);
                
                if(incType.equals("inc")){
                    String computed = compute(id.idValue(), "+", toInc);
                    id.setIdValue(computed);
                    id.setPrintValue(computed);
                    
                } else if(incType.equals("dec")){
                    String computed = compute(id.idValue(), "+", toInc);
                    id.setIdValue(computed);
                    id.setPrintValue(computed);
                    
                }
                
            } else {
                System.out.println("Error! " + id.lexeme() + " not declared.");
            }
        }
        
        if(element.type().equals(Token.TYPE_BIN_CONSTANT)){
            Token id = SymbolTable.table.get(currentId);
            
            if(!id.idType().equals("")){
                String toInc = convert(10, element.lexeme().substring(0, element.lexeme().length()-2), 2);
                
                if(incType.equals("inc")){
                    String computed = compute(id.idValue(), "+", toInc);
                    id.setIdValue(computed);
                    id.setPrintValue(computed);
                    
                } else if(incType.equals("dec")){
                    String computed = compute(id.idValue(), "+", toInc);
                    id.setIdValue(computed);
                    id.setPrintValue(computed);
                    
                }
                
            } else {
                System.out.println("Error! " + id.lexeme() + " not declared.");
            }
        }
    }
    
    public void forStatement(){
        Token element = currentNode.element();
        
        if(element.type().equals(GrammarRule.VAR_INIT_STMT)
                && todoOnFor.equals("init")){
            traverse(TODO_INIT);
            todoOnFor = "boolexp";
        }
        
        if(element.type().equals(GrammarRule.VAR_INC_STMT)
                && todoOnFor.equals("inc")){
            
            traverse(TODO_INC);
            todoOnFor = "boolexp";
            currentNode = forNode;
        }
        
        if(element.type().equals(GrammarRule.VAR_BLOCK)
                && todoOnFor.equals("block")){
            if(forCondition){
                
                traverse("");
                todoOnFor = "inc";
                currentNode = forNode;
                
            } else{
                todoOnFor = "init";
                bctr = -1;
            }
        }
        
        if (element.type().equals(GrammarRule.VAR_BOOLEAN_EXP)
                && todoOnFor.equals("boolexp")) {
            if(bctr == -1 || bctr == 2){
                
                Tree<Token> tree = new Tree<Token>(currentNode);
                
                tree.toExpressionTree();

                String prefixExp = tree.printPreorderR(" S@ ");
                
                forCondition = Boolean.parseBoolean(evalPrefix(prefixExp));
                todoOnFor = "block";
                currentNode = forNode;
                
                bctr = 0;
            }
            
            bctr++;
        }
    }
    
    boolean f1 = false;
    boolean f2 = true;
    public void arraydecStatement(){
        Token element = currentNode.element();
        
        if(!f1){
            Tree<Token> tree = new Tree(currentNode);

            System.out.println(tree.printGrouped()); f1 = true;
        }
    }
    
    public void arrayinitStatement(){
        Token element = currentNode.element();
        
        if(!f2){
            Tree<Token> tree = new Tree(currentNode);

            System.out.println(tree.printGrouped()); f2 = true;
        }
        
    }
    
    public void traverse(String todo) {
        
        if (currentNode != null) {

            //do something here
            try{
                if (todo.equals("")) {
                    doSomething();
                }
                if (todo.equals(TODO_INIT)) {
                    initStatement();
                }
                if (todo.equals(TODO_DEC)) {
                    decStatement();
                }
                if (todo.equals(TODO_PRINT)) {
                    printStatement();
                }
                if (todo.equals(TODO_GET)) {
                    getStatement();
                }
                if (todo.equals(TODO_IF)) {
                    ifStatement();
                }
                if (todo.equals(TODO_ELSE)) {
                    elseStatement();
                }
                if (todo.equals(TODO_CHECK)) {
                    checkStatement();
                }
                if (todo.equals(TODO_CHECKBLOCK)) {
                    checkblockStatement();
                }
                if (todo.equals(TODO_WHILE)) {
                    whileStatement();
                }
                if (todo.equals(TODO_DOWHILE)) {
                    dowhileStatement();
                }
                if (todo.equals(TODO_FOR)) {
                    forStatement();
                }
                if (todo.equals(TODO_BREAK)) {
                    breakStatement();
                }
                if (todo.equals(TODO_INC)) {
                    incStatement();
                }
                if (todo.equals(TODO_ADEC)) {
                    arraydecStatement();
                }
                if (todo.equals(TODO_AINIT)) {
                    arrayinitStatement();
                }
            } catch (StackOverflowError e) {
                System.out.println("Error! Infinite loop detected.");
                currentNode = null;
            }

            try{
                currentChildren = currentNode.children();

                if (currentChildren != null) {
                    SLLNode<TreeNode<Token>> ptr = currentChildren.getHead();

                    while (ptr != null) {
                        currentNode = ptr.info();

                        traverse(todo);

                        ptr = ptr.next();
                    }
                }
            }catch (Exception e){
                
            }
        }
    }

    public String compute(String left, String op, String right) {
        if (op.equals("+")) {
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "0";
                    
                } else left = token.idValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.idValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }

            double num1 = 0;
            double num2 = 0;
            try{
                num1 = Double.parseDouble(left);
                num2 = Double.parseDouble(right);
                
            } catch(Exception e){
                System.out.println("Error! Type mismatch.");
                return "0";
            }

            return formatDouble(Double.toString(num1 + num2));
        }

        if (op.equals("-")) {
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "0";
                    
                } else left = token.idValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.idValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            double num1 = 0;
            double num2 = 0;
            try{
                num1 = Double.parseDouble(left);
                num2 = Double.parseDouble(right);
                
            } catch(Exception e){
                System.out.println("Error! Type mismatch.");
                return "0";
            }

            return formatDouble(Double.toString(num1 - num2));
        }

        if (op.equals("*")) {
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "0";
                    
                } else left = token.idValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.idValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            double num1 = 0;
            double num2 = 0;
            try{
                num1 = Double.parseDouble(left);
                num2 = Double.parseDouble(right);
                
            } catch(Exception e){
                System.out.println("Error! Type mismatch.");
                return "0";
            }

            return formatDouble(Double.toString(num1 * num2));
        }

        if (op.equals("/")) {
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "0";
                    
                } else left = token.idValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.idValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            double num1 = 0;
            double num2 = 0;
            try{
                num1 = Double.parseDouble(left);
                num2 = Double.parseDouble(right);
                
            } catch(Exception e){
                System.out.println("Error! Type mismatch.");
                return "0";
            }
            
            if(num2 != 0)
                return formatDouble(Double.toString(num1 / num2));
            else {
                System.out.println("Error! Division by zero.");
                return "0";
            }
        }

        if (op.equals("^")) {
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "0";
                    
                } else left = token.idValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.idValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            double num1 = 0;
            double num2 = 0;
            try{
                num1 = Double.parseDouble(left);
                num2 = Double.parseDouble(right);
                
            } catch(Exception e){
                System.out.println("Error! Type mismatch.");
                return "0";
            }

            return formatDouble(Double.toString(Math.pow(num1, num2)));
        }

        if (op.equals("==")) {
            
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "0";
                    
                } else left = token.idValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.idValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            try{
                double num1 = Double.parseDouble(left);
                double num2 = Double.parseDouble(right);

                return Boolean.toString(num1 == num2);
            } catch(Exception e){
                return Boolean.toString(left.equals(right));
            }
        }

        if (op.equals("~=")) {
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "0";
                    
                } else left = token.idValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.idValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            try{
                double num1 = Double.parseDouble(left);
                double num2 = Double.parseDouble(right);

                return Boolean.toString(num1 != num2);
            } catch(Exception e){
                return Boolean.toString(!left.equals(right));
            }
        }

        if (op.equals(">")) {
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "0";
                    
                } else left = token.idValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.idValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            if(left.equals("true") && right.equals("false"))
                return "true";
            else if(left.equals("false") && right.equals("true"))
                return "false";
            else if(left.equals("true") && right.equals("true")
                    || left.equals("false") && right.equals("false"))
                return "false";

            try{
                double num1 = Double.parseDouble(left);
                double num2 = Double.parseDouble(right);

                return Boolean.toString(num1 > num2);
            } catch(Exception e){
                return Boolean.toString(left.compareTo(right) == 1);
            }
        }

        if (op.equals("<")) {
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "0";
                    
                } else left = token.idValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.idValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            if(left.equals("true") && right.equals("false"))
                return "false";
            else if(left.equals("false") && right.equals("true"))
                return "true";
            else if(left.equals("true") && right.equals("true")
                    || left.equals("false") && right.equals("false"))
                return "false";

            try{
                double num1 = Double.parseDouble(left);
                double num2 = Double.parseDouble(right);

                return Boolean.toString(num1 < num2);
            } catch(Exception e){
                return Boolean.toString(left.compareTo(right) == -1);
            }
        }

        if (op.equals("=<")) {
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "0";
                    
                } else left = token.idValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.idValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            if(left.equals("true") && right.equals("false"))
                return "false";
            else if(left.equals("false") && right.equals("true"))
                return "true";
            else if(left.equals("true") && right.equals("true")
                    || left.equals("false") && right.equals("false"))
                return "true";

            try{
                double num1 = Double.parseDouble(left);
                double num2 = Double.parseDouble(right);

                return Boolean.toString(num1 <= num2);
            } catch(Exception e){
                return Boolean.toString((left.compareTo(right) == -1)
                        || (left.equals(right)));
            }
        }

        if (op.equals("=>")) {
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "0";
                    
                } else left = token.idValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.idValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            if(left.equals("true") && right.equals("false"))
                return "true";
            else if(left.equals("false") && right.equals("true"))
                return "false";
            else if(left.equals("true") && right.equals("true")
                    || left.equals("false") && right.equals("false"))
                return "true";

            try{
                double num1 = Double.parseDouble(left);
                double num2 = Double.parseDouble(right);

                return Boolean.toString(num1 >= num2);
            } catch(Exception e){
                return Boolean.toString((left.compareTo(right) == 1)
                        || (left.equals(right)));
            }
        }

        if (op.equals("&")) {
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "0";
                    
                } else left = token.idValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.idValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            boolean l = Boolean.parseBoolean(left);
            boolean r = Boolean.parseBoolean(right);

            return Boolean.toString(l && r);
        }

        if (op.equals("|")) {
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "0";
                    
                } else left = token.idValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.idValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            boolean l = Boolean.parseBoolean(left);
            boolean r = Boolean.parseBoolean(right);

            return Boolean.toString(l || r);
        }

        if (op.equals("~")) {
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            boolean r = Boolean.parseBoolean(right);

            return Boolean.toString(!r);
        }

        if (op.equals(".")) {
            
            //if left is an id
            if(left.charAt(0) == '$') {
                Token token = SymbolTable.table.get(left);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    left = "";
                    
                } else left = token.printValue();
            //if left is hex or bin or oct
            } else if (left.charAt(left.length() - 1) == 'B'
                    || left.charAt(left.length() - 1) == 'b'
                    || left.charAt(left.length() - 1) == 'O'
                    || left.charAt(left.length() - 1) == 'o'
                    || left.charAt(left.length() - 1) == 'H'
                    || left.charAt(left.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(left);

                left = token.printValue();
            }
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "";
                    
                } else right = token.printValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.printValue();
            }
            
            if (left.charAt(left.length() - 1) == '"') {
                left = left.substring(0, left.length() - 1);
            }
            if (right.length() > 0 && right.charAt(0) == '"') {
                right = right.substring(1);
            }

            return left + right;
        }
        
        if(op.equals("totext")){
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            if(typeOf(right).equals(Token.DATA_TYPE_TEXT))
                return right;
            
            return "\"" + right + "\"";
        }
        
        if(op.equals("totnf")){
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            if(typeOf(right).equals(Token.DATA_TYPE_TNF))
                return right;
            
            if(right.equals("\"true\"") || right.equals("1")
                    || right.equals("true"))
                return "true";
            
            if(right.equals("\"false\"") || right.equals("0")
                    || right.equals("false"))
                return "false";
        }

        if (op.equals("tonum") || op.equals("tohex")
                || op.equals("tooct") || op.equals("tobin")) {
            
            //if right is an id
            if(right.charAt(0) == '$') {
                Token token = SymbolTable.table.get(right);

                if (token.idType().equals("")) {
                    System.out.println("Error! " + token.lexeme() + " not declared.");
                    
                    //TODO default value is 0.
                    right = "0";
                    
                } else right = token.idValue();
            //if right is hex or bin or oct
            } else if (right.charAt(right.length() - 1) == 'B'
                    || right.charAt(right.length() - 1) == 'b'
                    || right.charAt(right.length() - 1) == 'O'
                    || right.charAt(right.length() - 1) == 'o'
                    || right.charAt(right.length() - 1) == 'H'
                    || right.charAt(right.length() - 1) == 'h') {
                Token token = SymbolTable.table.get(right);

                right = token.idValue();
            }
            
            //from tnf
            if (right.equals("true")) {
                return "1";
            }
            if (right.equals("false")) {
                return "0";
            }

            //from text
            if(typeOf(right).equals(Token.DATA_TYPE_TEXT)){
                try{
                    double toRet = Double.parseDouble(right.substring(1, right.length()-1));
                    return Double.toString(toRet);
                } catch(Exception e){
                    System.out.println("Error! Cannot cast to num.");
                }
            }
            
            return right;
        }

        return "";
    }

    public String typeOf(String s) {
        if (isNumber(s)) {
            return Token.DATA_TYPE_NUM;
        }
        if (s.equals("true") || s.equals("false")) {
            return Token.DATA_TYPE_TNF;
        }
        if (s.charAt(s.length() - 1) == 'B'
                || s.charAt(s.length() - 1) == 'b') {
            return Token.DATA_TYPE_BIN;
        }
        if (s.charAt(s.length() - 1) == 'O'
                || s.charAt(s.length() - 1) == 'o') {
            return Token.DATA_TYPE_OCT;
        }
        if (s.charAt(s.length() - 1) == 'H'
                || s.charAt(s.length() - 1) == 'h') {
            return Token.DATA_TYPE_HEX;
        }
        return Token.DATA_TYPE_TEXT;
    }

    public boolean isNumber(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isDigit(s.charAt(i)) && s.charAt(i) != '.'
                    && s.charAt(i) != '-') {
                return false;
            }
        }
        return true;
    }

    public boolean isConstant(Token token) {
        return token.type().equals(Token.TYPE_LITERAL)
                || token.type().equals(Token.TYPE_NUMERIC)
                || token.type().equals(Token.TYPE_MAYBE_OCT)
                || token.type().equals(Token.TYPE_MAYBE_BIN)
                || token.type().equals(Token.TYPE_NUMERIC_FLOAT)
                || token.type().equals(Token.TYPE_HEX_CONSTANT)
                || token.type().equals(Token.TYPE_OCT_CONSTANT)
                || token.type().equals(Token.TYPE_BIN_CONSTANT)
                || token.type().equals(Token.TYPE_TRUE)
                || token.type().equals(Token.TYPE_FALSE);
    }

    public boolean isOperator(Token token) {
        return token.type().equals(Token.TYPE_ADD_SUBTRACT)
                || token.type().equals(Token.TYPE_MULTIPLY_DIVIDE)
                || token.type().equals(Token.TYPE_EXPONENTIAL)
                || token.type().equals(Token.TYPE_RELATIONAL)
                || token.type().equals(Token.TYPE_AND_NAND)
                || token.type().equals(Token.TYPE_OR_NOR)
                || token.type().equals(Token.TYPE_NOT)
                || token.type().equals(Token.TYPE_STRING_CONCAT);
    }

    public boolean isOperator(String op) {
        return op.equals("+") || op.equals("-")
                || op.equals("*") || op.equals("/")
                || op.equals("^") || op.equals("~")
                || op.equals("==") || op.equals("~=")
                || op.equals("<") || op.equals(">")
                || op.equals("=>") || op.equals("=<")
                || op.equals("&") || op.equals("|")
                || op.equals(".")
                || (op.length() > 2 && op.substring(0, 2).equals("to"));
    }

    public boolean isUnary(String op) {
        return op.equals("~")
                || op.equals("tonum")
                || op.equals("totext")
                || op.equals("totnf")
                || op.equals("tobin")
                || op.equals("tohex")
                || op.equals("tooct");
    }

    public String evalPrefix(String zzz) {
        String[] Y = zzz.split((" S@ "));

        ArrayList<String> toX = new ArrayList<String>();

        for (int i = 0; i < Y.length; i++) {
            if (Y[i].equals(" ") || Y[i].equals("")) {
                continue;
            } else {
                toX.add(Y[i]);
            }
        }

        String[] X = new String[toX.size()];

        for (int i = 0; i < toX.size(); i++) {
            X[i] = toX.get(i);
        }

        Stack<String> s = new Stack<String>();

        String a = "", b = "";

        for (int i = X.length - 1; i >= 0; i--) {
            if (!isOperator(X[i])) {
                s.push(X[i]);
            } else {
                if (!isUnary(X[i])) {
                    a = s.pop();
                } else {
                    a = "";
                }
                b = s.pop();
                s.push(compute(a, X[i], b));
            }
        }

        return s.pop();
    }

    public void printTable() {
        System.out.println();
        System.out.println("Table: ");

        Object[] keys = SymbolTable.table.keySet().toArray();

        for (int i = 0; i < keys.length; i++) {
            System.out.println(SymbolTable.table.get(keys[i]).properties());
        }
    }

    public String convert(int baseToConvertTo, String input, int baseOfNumber) {
        int[] rem = new int[1000];
        int num, i, ans, base, j, b;
        String inp;
        double n = 0, bin = 0;
        StringBuffer sk = new StringBuffer("");
        base = baseToConvertTo;
        inp = input;
        b = baseOfNumber;
        num = 0;
        for (i = inp.length() - 1; i >= 0; i--) {
            if (b >= 16) {
                if (Character.isLetter(inp.charAt(i))) {
                    switch (inp.charAt(i)) {
                        case 'A':
                            bin = 10;
                            break;
                        case 'B':
                            bin = 11;
                            break;
                        case 'C':
                            bin = 12;
                            break;
                        case 'D':
                            bin = 13;
                            break;
                        case 'E':
                            bin = 14;
                            break;
                        case 'F':
                            bin = 15;
                            break;
                    }
                }

                if (Character.isDigit(inp.charAt(i))) {
                    bin = (int) (inp.charAt(i)) - 48;
                }
            } else {
                bin = (int) (inp.charAt(i)) - 48;
            }
            num += bin * Math.pow(b, n);
            n++;
        }

//        System.out.printf ("Number\tBase\tAnswer\tRemainder\n") ;
        ans = 2;
        for (i = 0; ans > 0; i++) {
            ans = num / base;
            rem[i] = num % base;
//            System.out.printf ("%d\t\t%d\t\t%d\t\t%d\n", num, base, ans, rem[i]);
            num = ans;
        }
        i--;
        for (j = i; i >= 0; i--) {
            if (rem[i] > 9) {
                switch (rem[i]) {
                    case 10:
                        sk.append("A");
                        break;
                    case 11:
                        sk.append("B");
                        break;
                    case 12:
                        sk.append("C");
                        break;
                    case 13:
                        sk.append("D");
                        break;
                    case 14:
                        sk.append("E");
                        break;
                    case 15:
                        sk.append("F");
                        break;
                }
            } else {
                sk.append(Integer.toString(rem[i]));
            }
        }

        return sk.toString();
    }

    public TreeNode<Token> getNextSibling(TreeNode<Token> node) {

        SLL<TreeNode<Token>> children = node.parent().children();

        if (children != null) {

            SLLNode<TreeNode<Token>> ptr = children.getHead();

            while (ptr != null) {

                if (ptr.info().equals(node)) {
                    return ptr.next().info();
                }

                ptr = ptr.next();
            }
        }

        return null;
    }

    public String formatDouble(String str) {
        String[] strings = new String[2];

        strings[0] = "";
        strings[1] = "";

        int ix = 0;

        for (int i = 0; i < str.length(); i++) {

            if (str.charAt(i) == '.') {
                //'.' was passed. change destination of characters
                ix = 1;
                continue;
            }
            strings[ix] += str.charAt(i);
        }
        if(strings[1].equals("")) return strings[0];
        
        int decimalPart = Integer.parseInt(strings[1]);
        if (decimalPart == 0) {
            return strings[0];
        } else {
            return str;
        }
    }

}
