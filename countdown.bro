
	\c
	 c Countdown. In hexa. 
	 c With repeat. DUH.
	 c\

\\yow
progname #countdown
commence (:
	num $number, $repeats\e

	println("Enter a number")\e
	get $number\e
                
        print("How many times should the countdown")\e
	print(" be repeated?")\e
	println("")\e
        get $repeats\e

	do(:

                $temp = $number\e

                println("while")\e
		while($temp > 0)(:
                    dec $temp by 1\e

                    println($temp)\e
		:)

                println("for")\e
                for(num $i = 0, $i < $number, inc $i by 1)(:
                    println($i)\e
                :)
                
		dec $repeats by 1\e
		
	:)while($repeats > 0)
:)
end
